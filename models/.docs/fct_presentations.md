

{% docs presentation_name %}
Name given to the presentation.
{% enddocs %}

{% docs presentation_created_at %}
Timestamp of when the presentation was created.
{% enddocs %}

{% docs presentation_owner_id %}
Identifier for the owner of the presentation.
{% enddocs %}

{% docs online_count_time %}
Timestamp marking when the online count was recorded.
{% enddocs %}


{% docs is_team_play %}
Indicates whether the presentation is set up for team play (True/False).
{% enddocs %}

{% docs is_audience_pacing %}
Specifies if the presentation is paced by the audience (True/False).
{% enddocs %}

{% docs is_hide_individual_leaderboard %}
Indicates if individual leaderboards are hidden in the presentation (True/False).
{% enddocs %}

{% docs is_quiz_music %}
Denotes the inclusion of quiz music in the presentation (True/False).
{% enddocs %}

{% docs is_sample_responses %}
Specifies whether sample responses are included (True/False).
{% enddocs %}

{% docs is_private_feedback %}
Indicates if private feedback is enabled for the presentation (True/False).
{% enddocs %}

{% docs is_streak_bonus %}
Specifies whether a streak bonus is applied in the presentation (True/False).
{% enddocs %}

{% docs audio_name %}
Name of the audio file associated with the presentation, if any.
{% enddocs %}

{% docs is_chat %}
Indicates the availability of chat feature in the presentation (True/False).
{% enddocs %}

{% docs is_created_from_template %}
Indicates if the presentation was created from a template (True/False).
{% enddocs %}

{% docs is_created_from_community_template %}
Specifies if the presentation was created using a community template (True/False).
{% enddocs %}

{% docs thumbnail_type %}
Type of thumbnail used for the presentation.
{% enddocs %}

{% docs is_changed_font %}
Indicates whether the font in the presentation was changed (True/False).
{% enddocs %}

{% docs is_imported_slides %}
Specifies if slides were imported into the presentation (True/False).
{% enddocs %}

{% docs is_invited_user %}
Indicates if the user was invited to the presentation (True/False).
{% enddocs %}

{% docs is_thumbnail_uploaded %}
Specifies whether a thumbnail was uploaded for the presentation (True/False).
{% enddocs %}

{% docs is_thumbnail_changed %}
Indicates if the presentation's thumbnail was changed after creation (True/False).
{% enddocs %}
