
{% docs firstname %}
 Stores the first name of the user.
{% enddocs %}

{% docs lastname %}
 Stores the last name of the user.
{% enddocs %}

{% docs user_email %}
User email address 
{% enddocs %}

{% docs team_role %}
 Indicates the role of the user within their organization enterprise plan 
{% enddocs %}

{% docs team_id %}
 Stores the unique identifier of the users team or account.
{% enddocs %}

{% docs current_plan %}
User's current team plan. If null, user is not part of an enterprise purchase plan
{% enddocs %}

{% docs user_country %}
 Represents the country of the user.
{% enddocs %}

{% docs user_city %}
 Represents the city where the user is located.
{% enddocs %}

{% docs last_event_date %}
 Represents the date of the last recorded event for the user.
{% enddocs %}

{% docs events_count %}
 Stores the total count of events associated with the user.
{% enddocs %}

{% docs average_event_slidecount %}
 Indicates the average number of slides in the users 
{% enddocs %}

{% docs average_event_participants %}
 Represents the average number of participants in the users 
{% enddocs %}

{% docs most_event_participants %}
 Represents the maximum number of participants in any of the users 
{% enddocs %}

{% docs last_presentation %}
 Represents the date of the last presentation made by the user.
{% enddocs %}

{% docs presentations_count %}
 Stores the total count of presentations made by the user.
{% enddocs %}

{% docs average_presentation_slidecount %}
 Indicates the average number of slides in the users presentations.
{% enddocs %}

{% docs average_presentation_participants %}
The average audience size of users
{% enddocs %}

{% docs max_participants %}
The largest audience count of the user
{% enddocs %}

{% docs last_subscription %}
 Stores information about the users last subscription.
{% enddocs %}

{% docs last_payment_date %}
 Represents the date of the users last payment.
{% enddocs %}

{% docs last_subscription_end_date %}
 Indicates the end date of the users last subscription plan.
{% enddocs %}

{% docs total_paid_order %}
 Stores the total count of paid orders associated with the user.
{% enddocs %}

{% docs total_full_paid_order %}
 Stores the total count of fully paid orders associated with the user.
{% enddocs %}

{% docs total_discounted_order %}
 Stores the total count of discounted orders associated with the user.
{% enddocs %}

{% docs total_refunded_order %}
 Stores the total count of refunded orders associated with the user.
{% enddocs %}

{% docs total_amount_paid %}
 Represents the total amount paid by the user across all orders.
{% enddocs %}

{% docs total_amount_refunded %}
 Represents the total amount refunded to the user across all orders.
{% enddocs %}

{% docs total_life_time_value %}
 Represents the total lifetime value of the user based on their orders.
{% enddocs %}


{% docs is_organic %}
 Indicates whether the users source is organic (true/false).
{% enddocs %}

{% docs is_paid %}
 Indicates whether the user has made any paid transactions (true/false).
{% enddocs %}