{% docs payment_failure_id %}
 Represents the unique identifier for a payment failure record.
{% enddocs %}


{% docs plan_selected %}
 Stores the selected subscription plan at the time of the payment failure.
{% enddocs %}

{% docs createdat %}
 Indicates the date and time when the payment failure record was created.
{% enddocs %}

{% docs updatedat %}
 Indicates the date and time when the payment failure record was last updated.
{% enddocs %}

{% docs failure_transaction_amount %}
 Represents the amount of the failed payment transaction.
{% enddocs %}

{% docs error_message %}
 Stores an error message or description associated with the payment failure.
{% enddocs %}