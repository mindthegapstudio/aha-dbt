{% docs user_id %}
Represents the unique identifier of the user associated with the data or record.
{% enddocs %}

{% docs presentation_id %}
Refers to the unique identifier of the presentation used in the session. Links a session to a specific presentation within Ahaslides.
{% enddocs %}

{% docs online_count %}
Represents the number of online participants during the session. It includes all participants who joined the session virtually.
{% enddocs %}

{% docs participant_count %}
The total count of participants in the session. This includes both online and in-person participants, if applicable.
{% enddocs %}

{% docs sign_up_date %}
The date when users signed up for Ahaslides. This serves as the starting point for tracking user engagement and conversion metrics.
{% enddocs %}
