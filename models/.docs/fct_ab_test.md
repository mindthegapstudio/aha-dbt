{% docs test_code %}
Name of the test being done 
{% enddocs %}

{% docs variant %}
Whether the user is in treatment or control group
{% enddocs %}

{% docs test_name %}
Similar to test_code
{% enddocs %}

