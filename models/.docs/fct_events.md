{% docs session_id %}
The unique identifier for each session. This ID differentiates individual sessions hosted on Ahaslides.
{% enddocs %}


{% docs presenter_id %}
The unique identifier for the presenter or host of the session. This ID associates the session with the specific user who hosted it on Ahaslides.
{% enddocs %}

{% docs start_time %}
The timestamp indicating when the session started. This helps in identifying the exact date and time of the session's commencement.
{% enddocs %}


{% docs user_subscription %}
Describes the subscription type of the user who hosted the session. This could include various levels or types of Ahaslides subscriptions.
{% enddocs %}

{% docs last_event_time %}
Timestamp of the last event or activity within the session. This marks the most recent interaction or update during the session.
{% enddocs %}

{% docs event_ranking_desc %}
A numeric ranking of the session in descending order, possibly based on factors like participant count, engagement level, or other metrics.
{% enddocs %}

{% docs event_ranking_asc %}
A numeric ranking of the session in ascending order. It could be based on similar factors as `event_ranking_desc` but in the reverse order.
{% enddocs %}

{% docs is_retended %}
Boolean value indicating whether the session was retained for future reference or analysis. True if retained, false otherwise.
{% enddocs %}

{% docs use_ahaslides_for %}
A special data type capturing extensive details about how Ahaslides was utilized during the session. This might include usage patterns, features used, and other relevant information.
{% enddocs %}

{% docs hosting_subscription %}
Specifies the subscription type of the entity or individual hosting the session. This can differ from the `user_subscription` if the hosting responsibilities are delegated or shared.
{% enddocs %}