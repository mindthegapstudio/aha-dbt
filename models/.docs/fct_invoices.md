

{% docs invoice_id %}
The unique identifier for each invoice. This ID is essential for tracking and managing individual invoices.
{% enddocs %}

{% docs payment_status %}
Describes the current status of the payment, such as 'paid', 'pending', or 'refunded'. This status is critical for financial reconciliations and tracking.
{% enddocs %}

{% docs subscription_plan %}
Indicates the subscription plan associated with the invoice. This details the level or type of service the user is billed for.
{% enddocs %}

{% docs amount_paid %}
The total amount paid by the user as indicated in the invoice. This figure represents the gross payment before any refunds.
{% enddocs %}

{% docs amount_refunded %}
The total amount refunded to the user, if applicable. This metric is crucial for understanding net revenue and customer satisfaction issues.
{% enddocs %}

{% docs actual_paid %}
Represents the net amount paid by the user after accounting for any refunds. This figure reflects the actual revenue generated from the invoice.
{% enddocs %}

{% docs billing_reason %}
The reason or basis for the billing, such as 'new subscription', 'renewal', or 'upgrade'. This provides context for the payment.
{% enddocs %}

{% docs subscription_group %}
Categorizes the user into a subscription group, which can be used for segmenting users based on their subscription characteristics.
{% enddocs %}

{% docs payment_date %}
The date on which the payment was made. This timestamp is essential for financial tracking and reporting.
{% enddocs %}

{% docs subscription_end_date %}
Indicates when the current subscription plan is set to expire. This is important for understanding user lifecycle and subscription renewals.
{% enddocs %}

{% docs order_ranking_asc %}
A numeric rank assigned to the invoice in ascending order, potentially based on factors like amount, date, or user activity.
{% enddocs %}

{% docs order_ranking_desc %}
A numeric rank assigned to the invoice in descending order. Similar to 'order_ranking_asc' but in the reverse order.
{% enddocs %}

{% docs next_order_status %}
Describes the expected status of the user's next order retention status. This helps in predicting future revenue and user engagement.
{% enddocs %}