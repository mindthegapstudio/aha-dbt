
{% docs same_date_last_year %}
Corresponding date to 'sign_up_date' but from the previous year. Useful for year-over-year (YoY) comparisons of user engagement metrics.
{% enddocs %}

{% docs total_sign_up %}
The total number of user sign-ups on Ahaslides as of the 'sign_up_date'. This metric is crucial for analyzing the user acquisition funnel.
{% enddocs %}

{% docs total_paid_from_signup_7days %}
The number of users who signed up and converted to a paid subscription within 7 days. This measures the effectiveness of early conversion strategies.
{% enddocs %}

{% docs total_event_from_signup_7days %}
The count of users who organized or participated in an event on Ahaslides within 7 days of signing up. Indicates early user engagement.
{% enddocs %}

{% docs total_created_presentation_users %}
Total number of users who created a presentation on Ahaslides. Reflects user engagement with the core feature of the platform.
{% enddocs %}

{% docs total_hosted_event_user %}
The number of users who have hosted an event on Ahaslides. This metric helps in understanding the platform's usage for event hosting.
{% enddocs %}

{% docs total_paid_user %}
The total number of users who have upgraded to a paid subscription on Ahaslides. A key metric for tracking monetization success.
{% enddocs %}

{% docs yoy_total_paid_from_signup_7days %}
Year-over-year comparison of users converting to paid subscriptions within 7 days of sign-up. Helps in assessing the change in conversion rates.
{% enddocs %}

{% docs yoy_total_event_from_signup_7days %}
Year-over-year data showing how many users engaged in an event within 7 days of signing up. Useful for comparing engagement trends.
{% enddocs %}

{% docs yoy_total_sign_up %}
Year-over-year comparison of total user sign-ups. Indicates the growth or decline in new user acquisition over time.
{% enddocs %}

{% docs yoy_total_created_presentation_users %}
Compares the year-over-year data for users who created presentations. This metric reflects changes in user engagement with content creation features.
{% enddocs %}

{% docs yoy_total_hosted_event_user %}
Year-over-year comparison of users who hosted events. This helps in understanding the evolving usage of Ahaslides for event hosting.
{% enddocs %}

{% docs yoy_total_paid_user %}
Year-over-year comparison of the total paid users. Critical for evaluating the long-term growth in the platform's monetization.
{% enddocs %}