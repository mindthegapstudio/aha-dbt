{{ config(materialized='incremental',
 unique_key='slide_id',
 tags = ["int"]) }}

with base_slides as (
    select * from {{ ref('base_slides') }}
    {% if is_incremental() %}
        where updated_at >= (select date_add('day', -1, current_date))
    {% endif %}
),

int_ai_slides as (
    select * from {{ ref('int_ai_slides') }}
    {% if is_incremental() %}
        where request_created_at >= (select date_add('day', -1, current_date))
    {% endif %}
),

int_freestyle_slides as (
    select * from {{ ref('int_freestyle_slides') }}
),

combined_slides as (
    select
        bs.slide_id,
        bs.is_deleted,
        bs.updated_at,
        bs.created_at,
        bs.creator_id,
        bs.title,
        bs.slide_type,
        bs.presentation_id,
        bs.description,
        bs.openendedmultipleoption,
        bs.subheading,
        bs.maxpoint,
        bs.minpoint,
        bs.hastimelimit,
        bs.showvotes,
        bs.showsubmissions,
        bs.sourceslideid,
        bs.showvotingresultsonaudience,
        -- include other fields from base_slides as needed
        ais.request_id,
        ais.user_id,
        ais.request_content,
        ifs.is_imported_slides,
        -- include other fields from int_ai_slides as needed
        coalesce(ais.is_ai_imported, false) as is_ai_slides
--        case
--            when ais.is_ai_imported = true then true
--            else false
--        end as is_ai_slides
        -- include other fields from int_freestyle_slides as needed
    from base_slides as bs
    left join int_ai_slides as ais on bs.slide_id = ais.slide_id
    left join int_freestyle_slides as ifs on bs.slide_id = ifs.slide_id

)

select * from combined_slides
