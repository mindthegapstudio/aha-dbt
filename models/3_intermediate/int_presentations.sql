{{ config(materialized='incremental',
 unique_key='presentation_id',
 tags = ["int"]) }}


with presentation as (
    select * from {{ ref('base_presentations') }}
    {% if is_incremental() %}
        where updated_at >= (select date_add('day', -1, current_date))
    {% endif %}

),

slides as (
    select * from {{ ref('int_slides_final') }}
    {% if is_incremental() %}
        where updated_at >= (select date_add('day', -1, current_date))
    {% endif %}
),

from_template as (
    select
        presentation_id,
        user_id
    from presentation
    where is_public_search is true
    and is_private_mode is false
    and is_deleted is false
    and user_id in (1189137,1191742,1197617,1188997,1169101,559948,1070984)
),

from_community as (
    select
        presentation_id,
        user_id
    from presentation
    where is_public_search is true
    and is_private_mode is false
    and is_deleted is false
    and public_source = 'presenter'
    and user_id not in (1189137,1191742,1197617,1188997,1169101,559948,1070984)
),

from_copy as (
    select
        presentation_id,
        user_id
    from presentation
    where is_public_search is false
    and is_private_mode is false
    and is_deleted is false
),

slides_summarize as (
    select * from (
        select
            presentation_id,
            slide_type,
            count(distinct slide_id) as total_slide
        from slides
        group by 1, 2
    )
    pivot (
        sum(total_slide) as count for slide_type in
        (
            'bulletPoints',
            'correctOrder',
            'document',
            'freestyle',
            'googleSlides',
            'ideas',
            'image',
            'imageChoice',
            'leaderboard',
            'matchPairs',
            'multipleChoice',
            'openEnded',
            'pickAnswer',
            'qrcode',
            'question',
            'scale',
            'spinnerWheel',
            'textSlide',
            'typeAnswer',
            'wordCloud',
            'youtube'

        )
    )
),

slides_total as (
    select
        presentation_id,
        count(distinct slide_id) as total_slides,
        bool_or(is_ai_slides) as is_used_ai
    from slides
    group by 1
),

int_presentation as (

    select
        presentation.presentation_id,
        presentation.presentation_name,
        presentation.updated_at,
        presentation.team_id,
        presentation.created_at as presentation_created_at,
        presentation.user_id as presentation_owner_id,
        presentation.is_team_play,
        presentation.is_audience_pacing,
        presentation.is_hide_individual_leaderboard,
        presentation.is_quiz_music,
        presentation.is_sample_responses,
        -- presentation.is_qna_all_slide
        presentation.is_private_feedback,
        presentation.is_streak_bonus,
        presentation.audio_name,
        presentation.is_chat,
        presentation.online_count,
        presentation.deleted_at,
        presentation.is_private_mode,
        presentation.is_deleted,
        presentation.public_source,
        presentation.font_family,
        presentation.thumbnail_image,
        presentation.online_count_time,
        presentation.numberoflikes,
        presentation.numberofhearts,
        presentation.numberoflaughs,
        presentation.numberofsads,
        presentation.numberofwows,
        ss.bulletpoints_count,
        ss.correctorder_count,
        ss.document_count,
        ss.freestyle_count,
        ss.googleslides_count,
        ss.ideas_count,
        ss.image_count,
        ss.imagechoice_count,
        ss.leaderboard_count,
        ss.matchpairs_count,
        ss.multiplechoice_count,
        ss.openended_count,
        ss.pickanswer_count,
        ss.qrcode_count,
        ss.question_count,
        ss.scale_count,
        ss.spinnerwheel_count,
        ss.textslide_count,
        ss.typeanswer_count,
        ss.wordcloud_count,
        ss.youtube_count,
        st.total_slides,
        st.is_used_ai,
        coalesce(online_count >= 4, false) as is_event,
        coalesce(ft.presentation_id is not null, false) as is_created_from_template,
        coalesce(fc.presentation_id is not null, false) as is_created_from_community_template,
        coalesce(fo.presentation_id is not null, false) as is_created_from_other_presentation,
--        coalesce(
--            presentation.is_public_search = true
--            and is_private_mode = false
--            and is_deleted = false, false
--        ) as is_created_from_template,
--        coalesce(
--            presentation.is_public_search = true
--            and is_private_mode = false
--            and is_deleted = false
--            and public_source = 'presenter',
--            false
--        ) as is_created_from_community_template,
        case
            when thumbnail_image is null then 'no thumbnail'
            when thumbnail_image like 'static%' then 'custom_thumbnail'
            when thumbnail_image like 'themes%' then 'themes_thumbnail'
            else 'old_images'
        end as thumbnail_type,
        lag(online_count_time)
            over (
                partition by
                    presentation.user_id
                order by presentation.online_count_time
            )
        as last_event_time,
        not coalesce(lower(font_family) = 'nunito', false) as is_changed_font
    from presentation
    left join slides_summarize as ss
        on presentation.presentation_id = ss.presentation_id
    left join slides_total as st
        on presentation.presentation_id = st.presentation_id
    left join from_template as ft
        on presentation.source_presentation = ft.presentation_id
    left join from_community as fc
        on presentation.source_presentation = fc.presentation_id
    left join from_copy as fo
        on presentation.source_presentation = fo.presentation_id

)

select
    *,
    coalesce(thumbnail_type in (
        'custom_thumbnail', 'themes_thumbnail'
    ),
    false) as is_thumbnail_changed,
    coalesce(
        thumbnail_type = 'custom_thumbnail',
        false
    ) as is_thumbnail_uploaded
from int_presentation
