{{ config(materialized='table', unique_key='user_id',
tags = ["int"] )}}

with
users as (
    select *
    from {{ ref('base_users') }}
),

russian_hacker as (
    select * from {{ ref('russian_hacker') }}
),

teams as (
    select * from {{ ref('base_teams') }}
),


internal_user as (
    select * from {{ ref('int_internal_users') }}
),

public_domain as (
    select * from {{ ref('int_public_domain') }}
),

int_user as (
    select
        users.user_id,
        users.firstname,
        users.lastname,
        users.source,
        users.referraluserid as referred_by,
        presenterstartedat as sign_up_date,
        users.firstpage as first_page,
        users.personal_info,
        users.email as user_email,
        users.domain as user_domain,
        case
            when users.domain is not null and public_domain.domain is not null then 'Personal'
            when users.domain is not null and public_domain.domain is null then 'Work'
            else 'Others'
        end as user_domain_type,
--        case
--            when users.domain in (select domain from users) then 'Personal'
--            else 'Work'
--        end as user_domain_type,
        users.team_role,
        users.plan as current_plan,
        users.city as user_city,
        users.firsteventstartedat as first_event_date,
        teams.team_id,
        teams.team_name,
        teams.team_subscription,
        teams.plan_end as team_plan_endate,
        case
            when users.country = 'Russian Federation' then 'Russia'
            when users.country = 'Korea, Republic of' then 'South Korea'
            when users.country = 'Iran, Islamic Republic of' then 'Iran'
            else users.country
        end as user_country,
        coalesce(users.identifyprovider = 'skoletube', false) as is_skoletube,
        coalesce(
            users.personal_info.useahaslidesfor, 'not_mentioned'
        ) as use_ahaslides_for,
        coalesce(
            users.personal_info.userprofession, 'not_mentioned'
        ) as user_profession,
        coalesce(
            source like '%audience.ahaslides.com%'
            and personal_info is null,
            false
        ) as is_audience,
        coalesce(
            source like '%audience.ahaslides.com%'
            and presenterstartedat is not null,
            false
        ) as is_audience_converted,
        date_trunc('day', users.createdat) as account_creation_date,
        coalesce(internal_user.user_id is not null, false) as is_internal,
        coalesce(russian_hacker.user_id is not null, false) as is_hacker,

        case
            when (users.firstpage ilike '%/blog/%'
                or users.firstpage ilike '%/features/%'
                or users.firstpage ilike '%/templates/%'
                or users.firstpage ilike '%/events/%'
                or users.firstpage ilike 'https://ahaslides.com/%')
                and users.firstpage not ilike '%gclid%'
                and users.firstpage not ilike '%fbclid%'
                then 'Organic search or Direct'
            when users.firstpage ilike '%gclid%' then 'Google Ads Paid search'
            when users.firstpage ilike '%fbclid%' then 'Facebook search'
            when users.firstpage ilike 'https://audience.ahaslides.com%' then 'Audience app'
            when users.firstpage ilike 'https://presenter.ahaslides.com%' then 'Presenter app'
            when users.firstpage = '' then 'Unknown'
            else 'Others'
        end as first_page_group

--        case
--            when users.firstpage ilike '%/blog/%' then 'Blogs'
--            when users.firstpage ilike '%/features/%' then 'Features'
--            when users.firstpage ilike '%/templates/%' then 'Templates'
--            when users.firstpage ilike '%/events%' then 'Events'
--            when users.firstpage ilike '%/presenter.%' then 'Presenter app'
--            when users.firstpage ilike '%/pricing%' then 'Pricing'
--            when users.firstpage ilike '%//ahaslides.com/%'
--                and users.firstpage not ilike '%/blog/%'
--                and users.firstpage not ilike '%/features/%'
--                and users.firstpage not ilike '%/templates/%'
--                and users.firstpage not ilike '%/events%'
--                then 'Home page'
--            when users.firstpage ilike '%christmas%'
--                and users.firstpage ilike '%get.ahaslides.com%'
--                then 'Christmas'
--            when users.firstpage ilike '%get.ahaslides.com%'
--                and users.firstpage ilike '%back-to-work%' then 'Back to work'
--            when users.firstpage ilike '%get.ahaslides.com%'
--                and users.firstpage ilike '%enterprise%' then 'Enterprise'
--            when users.firstpage ilike '%get.ahaslides.com%'
--                and (users.firstpage ilike '%-vs-%'
--                or users.firstpage ilike '%/compare/%')
--                then 'Alternatives'
--            when users.firstpage ilike '%get.ahaslides.com%'
--                and users.firstpage ilike '%webinar%' then 'Webinar'
--            when users.firstpage ilike '%get.ahaslides.com%'
--                and users.firstpage ilike '%holiday%' then 'Holidays'
--            when users.firstpage ilike '%get.ahaslides.com%'
--                and users.firstpage ilike '%about-us%' then 'About us'
--            when users.firstpage ilike '%get.ahaslides.com%'
--                and users.firstpage ilike '%contact-us%' then 'Contact us'
--            when users.firstpage = 'https://get.ahaslides.com/'
--                then 'Back to homepage'
--            when users.firstpage ilike '%/audience.%' then 'Audience app'
--            when users.firstpage = '' then 'Unknown'
--            else 'Others'
--        end as first_page_group_break_down,

--        case
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and (users.firstpage ilike '%?utm_source=gg&utm_medium=search&utm_campaign=generalcomparison%'
--                or users.firstpage ilike '%?utm_source=gg&utm_medium=search&utm_campaign=glbcompetitor%')
--                then 'Search_GLB_general'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=search&utm_campaign=kahoot%'
--                then 'Search_GLB_Kahoot'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=search&utm_campaign=mentimeter%'
--                then 'Search_GLB_Menti'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=search&utm_campaign=slido%'
--                then 'Search_GLB_Slido'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=search&utm_campaign=quizizz%'
--                then 'Search_GLB_quizziz'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and (users.firstpage ilike '%?utm_source=gg&utm_medium=roas&utm_campaign=generalcomparison%'
--                or users.firstpage ilike '%?utm_source=gg&utm_medium=search&utm_campaign=glbroas%')
--                then 'Search_ROAS_general'
----            when users.firstpage ilike '%gad_source=1&gclid=%'
----                and users.firstpage ilike
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=roas&utm_campaign=kahoot%'
--                then 'Search_ROAS_kahoot'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=roas&utm_campaign=mentimeter%'
--                then 'Search_ROAS_menti'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=roas&utm_campaign=slido%'
--                then 'Search_ROAS_slido'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=roas&utm_campaign=quizizz%'
--                then 'Search_ROAS_quizziz'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=rmkt&utm_campaign=generalcomparison%'
--                then 'Search_remarket_general'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=rmkt&utm_campaign=kahoot%'
--                then 'Search_remarket_kahootl'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=rmkt&utm_campaign=mentimeter%'
--                then 'Search_remarket_menti'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=rmkt&utm_campaign=slido%'
--                then 'Search_remarket_slido'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%?utm_source=gg&utm_medium=rmkt&utm_campaign=quizziz%'
--                then 'Search_remarket_quizziz'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%utm_source=gg&utm_medium=search&utm_campaign=feature_word_cloud%'
--                then 'Search_WordCloud_campaign'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%utm_source=gg&utm_medium=search&utm_campaign=ROAS%'
--                then 'Search_ROAS_campaign'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%utm_source=gg&utm_medium=search&utm_campaign=mainkey%'
--                then 'Search_main_key_campaign'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%utm_source=yt&utm_medium=trainers&utm_campaign=branding&utm_content=video%'
--                then 'Youtube_branding_campaign'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage ilike '%utm_source=gg&utm_medium=search&utm_campaign=otherkey%'
--                then 'Search_other_keys_campaign'
--            when users.firstpage ilike '%gad_source=1&gclid=%'
--                and users.firstpage not ilike '%utm_source=gg&utm_medium=search&utm_campaign=ROAS%'
--                and users.firstpage not ilike '%utm_source=gg&utm_medium=search&utm_campaign=otherkey%'
--                and users.firstpage not ilike '%utm_source=yt&utm_medium=trainers&utm_campaign=branding&utm_content=video%'
--                and users.firstpage not ilike '%utm_source=gg&utm_medium=search&utm_campaign=mainkey%'
--                and users.createdat between to_date('2024-03-08', 'YYYY-MM-DD') and to_date('2024-05-16', 'YYYY-MM-DD')
--                then 'Search_MAX_Performance'
--        end as acquisition_campaign

    from users
    left join internal_user
        on users.user_id = internal_user.user_id
    left join public_domain
        on users.user_id = public_domain.user_id
    left join russian_hacker
        on users.user_id = russian_hacker.user_id
    left join teams
        on users.team_id = teams.team_id
    {% if is_incremental() %}
        and updated_at > (select  date_add('day', -1, current_date) )
   {% endif %}

)


select * from int_user
