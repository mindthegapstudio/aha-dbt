{{ config(materialized='table',
 unique_key='month_serries',
 tags = ["int"]) }}

with b2b_deal as (
    select
        cast(total_amount as real) / duration_monthx as mrr,
        type,
--        plan_start,
--        plan_end,
        plan_start_monthx as plan_start,
        plan_end_monthx as plan_end,
        is_churned
    from {{ ref('base_b2b_new') }}
    where
        1 = 1
),

date_spine as (
    select distinct
        date_trunc('month', date) as month_series
    from {{ ref('dim_date') }}
    where date >= cast('2019-07-01' as date)
      and date < dateadd('year', 1, current_date)
),

int_raw_mrr_b2b as (
    select
        date_spine.month_series,
        sum(case
                when date_spine.month_series = b2b_deal.plan_start
                    and b2b_deal.type = 'New'
                    then b2b_deal.mrr
                else 0
            end) as new_mrr,

        sum(case
                when date_spine.month_series = b2b_deal.plan_start
                    and b2b_deal.type = 'Expansion'
                    then b2b_deal.mrr
                else 0
            end) as expansion_mrr,

--        sum(case
--                when date_spine.month_series = b2b_deal.plan_end
----                date_add('month',1,b2b_deal.plan_end)
--                    and is_churned is true
--                    then -b2b_deal.mrr
--                else 0
--            end) as churned_mrr,

        sum(case
                when (date_spine.month_series > b2b_deal.plan_start
                        and date_spine.month_series < b2b_deal.plan_end
                        and b2b_deal.type = 'New')
                    or (b2b_deal.type = 'Renewal'
                        and date_spine.month_series >= b2b_deal.plan_start
                        and date_spine.month_series < b2b_deal.plan_end)
                    or (b2b_deal.type = 'Expansion'
                        and date_spine.month_series > b2b_deal.plan_start
                        and date_spine.month_series < b2b_deal.plan_end)
                    then b2b_deal.mrr
                else 0
            end) as recurring_mrr
    from date_spine
        left join b2b_deal
            on date_spine.month_series >= b2b_deal.plan_start
                and date_spine.month_series < b2b_deal.plan_end
    group by 1
)
--,
--
--int_raw_mrr_b2b as (
--    select
--        month_series,
--        coalesce(new_mrr,0) + coalesce(recurring_mrr,0) + coalesce(expansion_mrr,0) as total_mrr,
--        coalesce(new_mrr,0) + coalesce(expansion_mrr,0) + coalesce(churned_mrr,0) as net_new_mrr,
--        coalesce(new_mrr,0) as new_mrr,
--        coalesce(recurring_mrr,0) as recurring_mrr,
--        coalesce(expansion_mrr,0) as expansion_mrr,
--        coalesce(churned_mrr,0) as churned_mrr,
--        case
--            when (coalesce(new_mrr,0) + coalesce(recurring_mrr,0) + coalesce(expansion_mrr,0)) = 0 then 0
--            else round(cast(coalesce(-churned_mrr,0) as real) / (coalesce(new_mrr,0) + coalesce(recurring_mrr,0) + coalesce(expansion_mrr,0)),2)
--        end as churn_rate
--    from raw_b2b
--)

select * from int_raw_mrr_b2b
order by month_series desc