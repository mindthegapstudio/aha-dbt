{{ config(materialized='table',
 unique_key='payment_date',
 tags = ["int"]) }}

with invoices as (
    select
        user_id,
        invoice_id,
        payment_method,
        actual_paid,
        billing_reason,
        subscription_type,
        subscription_plan,
        subscription_group,
        plan_type,
        subscription_end_date as plan_end,
        payment_date,
        created_date as plan_start,
        case
            when billing_reason = 'subscription_update' then 'expansion'
            when billing_reason = 'subscription_cycle' then 'recurring'
            else 'new'
        end as category
--        subscription_duration_day
    from {{ ref('int_invoices_b2c') }}
    where
        1 = 1
        and actual_paid > 0
),

int_raw_mrr_b2c as (
    select
        *,
        datediff('month',plan_start,plan_end) as duration,
        case
            when datediff('month',plan_start,plan_end) = 0
                or subscription_group = 'one_time'
                then actual_paid
            else actual_paid / datediff('month',plan_start,plan_end)
        end as mrr
    from invoices
--    where subscription_group <> 'one_time'
)

--one_time as (
--    select
--        *,
--        1 as duration,
--        actual_paid / 1 as mrr
--    from invoices
--    where subscription_group = 'one_time'
--)

select * from int_raw_mrr_b2c
--union all
--select * from one_time