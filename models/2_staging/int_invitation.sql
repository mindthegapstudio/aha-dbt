{{ config(tags = ["staging"]) }}
with role_presentation as (
    select * from {{ ref('base_user_role_presentations') }}
),

role_folder as (
    select * from {{ ref('base_user_role_folder') }}
),

user_invitation as (
    select * from {{ ref('base_user_invitation') }}
),


user_role as (
    select * from {{ ref('base_role') }}
),

grouping_role as (
    select
        user_invitation.invitation_id,
        user_invitation.invited_id,
        user_invitation.invited_email,
        user_invitation.invited_name,
        user_invitation.invited_plan,
        role_folder.folder_id,
        role_presentation.presentation_id,
        user_invitation.inviter_id,
        user_invitation.created_at as role_created_date,
        user_invitation.sent_at as invitation_sent_date,
        user_invitation.claimed_at as invitation_claim_date,
        user_invitation.team_id,
        user_invitation.team_role,
        coalesce(role_presentation.role_id, role_folder.role_id) as role_id
    from user_invitation
    left join role_presentation
        on
            user_invitation.inviter_id = role_presentation.inviter_id
            and user_invitation.invited_id = role_presentation.invited_id
            and user_invitation.presentation_id
            = role_presentation.presentation_id
    left join role_folder
        on
            user_invitation.inviter_id = role_folder.inviter_id
            and user_invitation.invited_id = role_folder.invited_id
            and user_invitation.folder_id = role_folder.folder_id
)

select
    grouping_role.*,
    user_role.role_name
from grouping_role left join user_role
    on grouping_role.role_id = user_role.role_id
