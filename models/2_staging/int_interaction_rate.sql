
{{ config(materialized='table', tags = ["staging"]) }}
with answers as (
    select
        presentation_id,
        audience_id,
        count(answer_id) as total_answer
    from {{ ref('base_answers') }}
    group by 1, 2
),

audience as (
    select
        audience_id,
        presentation_id
    from {{ ref('base_audiences') }}
),

calculate as (
    select
        audience.audience_id,
        audience.presentation_id,
        coalesce(answers.total_answer > 0, false) as is_interaction
    from audience
    left join answers on
        audience.audience_id = answers.audience_id
        and audience.presentation_id = answers.presentation_id

)

select
    presentation_id,
    count(audience_id) as total_audience,
    count(
        case when is_interaction then audience_id end
    ) as interacted_audience
from calculate
group by 1
