
{{ config(materialized='table') }}

with base_ai_requests as (
    select * from {{ ref('base_ai_requests') }}
),

response as (
    select * from {{ ref('base_ai_responses') }}
),

response_to_slide as (
    select * from {{ ref('base_ai_response_to_slides') }}
),

language_slide as (
    select * from {{ ref('base_ai_language') }}
),

joined_data as (
    select
        r.request_id,
        r.user_id,
        r.content as request_content,
        r.type as request_type,
        r.createdat as request_created_at,
        r.openaimodel,
        r.openaiprompttokenscount,
        r.openaicompletiontokenscount,
        resp.response_id,
        resp.content,
        resp.response_content,
        resp.created_at as response_created_at,
        resp.responsetype as response_type,
        rs.slide_id,
        lang.input_language,
        lang.output_language,
        rs.created_at as slide_created_at,
        coalesce(resp.content is null, false) as is_stumped,
        coalesce(
            lang.input_language = lang.output_language, false
        ) as is_corrected_language,
        case
            when rs.slide_id is not null then true
            else false
        end as is_ai_imported,
--        coalesce(rs.slide_id is not null, false) as is_ai_imported,
        row_number()
            over (
                partition by resp.response_id, resp.request_id, r.createdat
                order by r.createdat desc
            )
        as rn

    from base_ai_requests as r
    left join response as resp on r.request_id = resp.request_id
    left join response_to_slide as rs on resp.response_id = rs.response_id
    left join language_slide as lang on resp.response_id = lang.response_id
)

select *
from joined_data
--where rn = 1
