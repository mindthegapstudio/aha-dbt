
{{ config(materialized='table',
 unique_key='session_id',
 tags = ["staging"]) }}

with presentation_session as (
    select * from {{ ref('base_presentation_sessions') }}
    {% if is_incremental() %}
        where updated_at >= (select date_add('day', -2, current_date))
    {% endif %}
),



event_filter as (
    select
        session_id,
        source,
        presentation_id,
        created_at,
        updated_at,
        presenter_id,
        start_time,
        online_count,
        participant_count,
        user_subscription,
        font_family,
        slide_count,
        is_qna_all_slide,
        is_enable_chat,
        is_anonymous_qna,
        is_show_hyperlink,
        is_audience_pacing,
        number_of_authentication_code_generated,
        team_scoring_rule,
        is_moderated_audience_question,
        is_show_ahaslides_cta,
        is_enable_quiz_music,
        is_reaction_enabled,
        is_show_ahaslides_logo,
        is_quiz_countdown_enabled,
        is_filtering_profanity,
        is_hide_instruction_bar,
        is_qna_audience_show_all,
        is_audience_authentication,
        is_enable_copy_slide_note,
        is_has_example_responses
        -- datediff(
        --     hour,
        --     first_value(start_time)
        --         over (
        --             partition by
        --                 presenter_id, presentation_id, trunc(start_time)
        --             order by
        --                 start_time
        --             rows between unbounded preceding and unbounded following
        --         ),
        --     start_time
        -- )
        -- as time_from_first_event,
        -- datediff(
        --     hour,
        --     lag(start_time)
        --         over (
        --             partition by
        --                 presenter_id, presentation_id, trunc(start_time)
        --             order by start_time
        --         ),
        --     start_time
        -- )
        -- as time_from_previous_event,
        -- row_number()
        --     over (
        --         partition by presenter_id, presentation_id, trunc(start_time)
        --         order by start_time asc
        --     )
        -- as same_event_row
    from presentation_session
    -- where
    --     online_count >= 4

)


select
    *
    -- Counting only the first events within a 3 hours period as is_events, for filtering down the line. 
from event_filter
where 1 = 1
