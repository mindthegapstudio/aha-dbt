
{{ config(materialized='table', unique_key='user_id', tags = ["staging"],
pre_hook="set statement_timeout to 120000") }}

with
users as (select distinct
    user_id,
    email,
    domain
    from {{ ref('base_users') }}
),

public_domain as (
    select user_id, email, domain
    from users
    WHERE
        domain ~~ '%gmail.%'::text
        OR domain ~~ '%hotmail.%'::text
        OR domain ~~ '%outlook.%'::text
        OR domain ~~ '%icloud.%'::text
        OR domain ~~ '%web.de%'::text
        OR domain ~~ '%mail.ru%'::text
        OR domain ~~ '%yahoo.%'::text
        OR domain ~~ '%googlemail.%'::text
        OR domain ~~ '%aol.%'::text
        OR domain ~~ '%live.%'::text
        OR domain ~~ '%me.com%'::text
        OR domain ~~ '%yandex.%'::text
        OR domain ~~ '%msn.%'::text
        OR domain ~~ '%live.co.%'::text
        OR domain ~~ '%t-online.de%'::text
        OR domain ~~ '%seznam.cz%'::text
        OR domain ~~ '%ymail.%'::text
        OR domain ~~ '%sky.com%'::text
        OR domain ~~ '%gamil.com%'::text
        OR domain ~~ '%ORange.fr%'::text
        OR domain ~~ '%gmx.%'::text
        OR domain ~~ '%libero.%'::text
        OR domain ~~ '%gmai.com%'::text
        OR domain ~~ '%naver.%'::text
        OR domain ~~ '%bluewin.%'::text
        OR domain ~~ '%mail.com%'::text
        OR domain ~~ '%wp.pl%'::text
        OR domain ~~ '%telenet.%'::text
        OR domain ~~ '%windowslive.%'::text
        OR domain ~~ '%gmaill.com%'::text
        OR domain ~~ '%gmaol.com%'::text
        OR domain ~~ '%aolp.%'::text
        OR domain ~~ '%wanadoo.%'::text
        OR domain ~~ '%ORange.%'::text
        OR domain ~~ '%comcast.net%'::text
        OR domain ~~ '%rediffmail.%'::text
        OR domain ~~ '%free.fr%'::text
        OR domain ~~ '%uol.com.%'::text
        OR domain ~~ '%bol.com.%'::text
        OR domain ~~ '%cox.net%'::text
        OR domain ~~ '%sbcglobal.net%'::text
        OR domain ~~ '%sfr.fr%'::text
        OR domain ~~ '%verizon.net%'::text
        OR domain ~~ '%ig.com.br%'::text
        OR domain ~~ '%bigpond.com%'::text
        OR domain ~~ '%terra.com.br%'::text
        OR domain ~~ '%neuf.fr%'::text
        OR domain ~~ '%alice.it%'::text
        OR domain ~~ '%rocketmail.%'::text
        OR domain ~~ '%att.net%'::text
        OR domain ~~ '%laposte.net%'::text
        OR domain ~~ '%facebook.%'::text
        OR domain ~~ '%bellsouth.net%'::text
        OR domain ~~ '%charter.net%'::text
        OR domain ~~ '%rambler.ru%'::text
        OR domain ~~ '%tiscali.%'::text
        OR domain ~~ '%shaw.ca%'::text
        OR domain ~~ '%earthlink.net%'::text
        OR domain ~~ '%optonline.net%'::text
        OR domain ~~ '%freenet.%'::text
        OR domain ~~ '%aliceadsl.fr%'::text
        OR domain ~~ '%virgilio.it%'::text
        OR domain ~~ '%home.nl%'::text
        OR domain ~~ '%qq.com%'::text
        OR domain ~~ '%voila.fr%'::text
        OR domain ~~ '%planet.nl%'::text
        OR domain ~~ '%tin.it%'::text
        OR domain ~~ '%ntlwORld.com%'::text
        OR domain ~~ '%arcOR.de%'::text
        OR domain ~~ '%frontiernet.net%'::text
        OR domain ~~ '%hetnet.nl%'::text
        OR domain ~~ '%zonnet.nl%'::text
        OR domain ~~ '%club-internet.fr%'::text
        OR domain ~~ '%juno.com%'::text
        OR domain ~~ '%optusnet.com.au%'::text
        OR domain ~~ '%blueyonder.co.uk%'::text
        OR domain ~~ '%bluewin.ch%'::text
        OR domain ~~ '%skynet.be%'::text
        OR domain ~~ '%sympatico.ca%'::text
        OR domain ~~ '%windstream.net%'::text
        OR domain ~~ '%centurytel.net%'::text
        OR domain ~~ '%chello.nl%'::text
        OR domain ~~ '%aim.com%'::text
        OR domain ~~ '%bigpond.net.au%'::text
--        OR domain ~~ '%deleted%'::text
        OR domain ~~ '%017.%'::text
        OR domain ~~ '%0101001.%'::text
        OR domain ~~ '%012.net%'::text
        OR domain ~~ '%013.net%'::text
        OR domain ~~ '%06.%'::text
        OR domain ~~ '%070909.%'::text
        OR domain ~~ '%123.gmail%'::text
        OR domain ~~ '%email.%'::text
        OR domain ~~ '%upcmail.%'::text
        OR domain ~~ '%onet.%'::text
        OR domain ~~ '%op.pl%'::text
        OR domain ~~ '%gmal.com%'::text
        OR domain ~~ '%126.com%'::text
        OR domain ~~ '%mailbox.%'::text
        OR domain ~~ '%gamail.%'::text
        OR domain ~~ '%notset.mail%'::text
        OR domain ~~ '%cuoly.com%'::text
        OR domain ~~ '%gnail.com%'::text
        OR domain ~~ '%zeelandnet.nl%'::text
        OR domain ~~ '%delta.nl%'::text
        OR domain ~~ '%pm.me%'::text
        OR domain ~~ '%quicknet.nl%'::text
        OR domain ~~ '%poczta.onet.%'::text
        OR domain ~~ '%gmali.%'::text
        OR domain ~~ '%btopenwORld.%'::text
        OR domain ~~ '%gimail.com%'::text
        OR domain ~~ '%gemail.com%'::text
        OR domain ~~ '%zizmail.%'::text
        OR domain ~~ '%mail.dk%'::text
        OR domain ~~ '%zwoho.%'::text
        OR domain ~~ '%proton.%'::text
        OR domain ~~ '%bluemail.%'::text
        OR domain ~~ '%inbox.%'::text
        OR domain ~~ '%hotmai.%'::text
        OR domain ~~ '%post.%'::text
        OR domain ~~ '%mail.nl%'::text
        OR domain ~~ '%hmail.com%'::text
        OR domain ~~ '%hotmil.com%'::text
        OR domain ~~ '%internet.ru%'::text
        OR domain ~~ '%kabelmail.de%'::text
        OR domain ~~ '%btinternet.%'::text
        OR domain ~~ '%google.%'::text
        OR domain ~~ '%protonmail.%'::text
        OR domain ~~ '%163.%'::text
        OR domain ~~ '%microsoft%'::text
        OR domain ~~ '%bk.ru%'::text
        OR domain ~~ '%ukr.net%'::text
        OR domain ~~ '%list.ru%'::text
        OR domain ~~ '%ziggo.%'::text
        OR domain ~~ '%online.%'::text
        OR domain ~~ '%yopmail.%'::text
        OR domain ~~ '%gmil.com%'::text
        OR domain ~~ '%fb.com%'::text
        OR domain ~~ '%ya.ru%'::text
        OR domain ~~ '%kpn.com%'::text
        OR domain ~~ '%gmailc%'::text
        OR domain ~~ '%gmal%'::text
        OR domain ~~ '%email.cz%'::text
        OR domain ~~ '%abv.bg%'::text
        OR domain ~~ '%lausd.net%'::text
        OR domain ~~ '%nhs.net%'::text
        OR domain ~~ '%o2.pl%'::text
        OR domain ~~ '%posteo.de%'::text
        OR domain ~~ '%suardino.com%'::text
        OR domain ~~ '%hanmail.%'::text
        OR domain ~~ '%nezid.com%'::text
        OR domain ~~ '%usd259.net%'::text
        OR domain ~~ '%vipmail.%'::text
        OR domain ~~ '%ghmail.com%'::text
        OR domain ~~ '%gmial.com%'::text
)

select * from public_domain