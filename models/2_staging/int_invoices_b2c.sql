
{{ config(materialized='table', sort='payment_date', unique_key='invoice_id', tags = ["staging"]) }}

with invoices as (
    select *,
            case
                when isenterprise = 'false'
                    and billingreason = 'manual'
                    and subscription_type not ilike '%One-Time%'
                    and paymentdate >= date('2024-01-01')
                    and payment_source = 'stripe'
                    then true
                else false
            end as is_b2b_on_stripe
    from {{ ref('base_invoices') }}
    where
        1 = 1
    and isenterprise = 'false'
),

internal_user as (
    select * from {{ ref('int_internal_users') }}
),

int_invoices as (
    select
        invoices.user_id,
        invoices.invoice_id,
        invoices.payment_status,
        invoices.payment_source,
        invoices.payment_method,
        invoices.quantity,
        invoices.amountpaid::float as amount_paid,
        invoices.amountrefunded::float as amount_refunded,
        invoices.billingreason as billing_reason,
        invoices.coupon_code,
        invoices.currency,
        invoices.exchangerate,
        invoices.subscription_type,
        coalesce(invoices.quantity > 1, false) as is_bulk,
        case
            when
                invoices.subscription_type ilike '%essential%'
                and invoices.subscription_type ilike '%monthly%'
                then 'Essential Monthly'
            when invoices.subscription_type ilike '%plus%'
                and invoices.subscription_type ilike '%monthly%'
                then 'Plus Monthly'
            when invoices.subscription_type ilike '%pro%'
                and invoices.subscription_type ilike '%monthly%'
                then 'Pro Monthly'
            when invoices.subscription_type ilike '%plus%'
                and invoices.subscription_type ilike '%yearly%'
                then 'Plus Yearly'
            when
                invoices.subscription_type ilike '%essential%'
                and invoices.subscription_type ilike '%yearly%'
                then 'Essential Yearly'
            when invoices.subscription_type ilike '%pro%'
                and invoices.subscription_type ilike '%yearly%'
                then 'Pro Yearly'
            when
                invoices.subscription_type ilike '%edu%'
                and invoices.subscription_type ilike '%small%'
                then 'Edu Small'
            when
                invoices.subscription_type ilike '%edu%'
                and invoices.subscription_type ilike '%medium%'
                then 'Edu Medium'
            when
                invoices.subscription_type ilike '%edu%'
                and invoices.subscription_type ilike '%large%'
                then 'Edu Large'
--            when
--                subscription_type = 'Edu Small'
--                or subscription_type = 'Edu (Small)'
--                then 'Edu Small'
--            when
--                subscription_type = 'Edu Medium'
--                or subscription_type = 'Edu (Medium)'
--                then 'Edu Medium'
--            when
--                subscription_type = 'Edu Large'
--                or subscription_type = 'Edu (Large)'
--                then 'Edu Large'
            when invoices.subscription_type ilike '%one-time%'
                and subscription_type ilike '%mini%'
                then 'One-Time Mini'
            when invoices.subscription_type ilike '%one-time%'
                and subscription_type ilike '%small%'
                then 'One-Time Small'
            when invoices.subscription_type ilike '%one-time%'
                and subscription_type ilike '%medium%'
                then 'One-Time Medium'
            when invoices.subscription_type ilike '%one-time%'
                and subscription_type ilike '%large%'
                then 'One-Time Large'
            when invoices.subscription_type ilike '%white%'
                then 'White Label'
            else 'others'
        end as subscription_plan,

        lead(invoices.billingreason)
            over (partition by invoices.user_id order by invoices.paymentdate)
        as next_billing_reason,

        lag(invoices.billingreason)
            over (partition by invoices.user_id order by invoices.paymentdate)
        as previous_billing_reason,

        invoices.amountpaid::float - invoices.amountrefunded::float as actual_paid,

        date_trunc('day', invoices.paymentdate) as payment_date,
        invoices.paymentdate as payment_date_raw,

        date_trunc('day',invoices.createdat) as created_date,

        coalesce(lead(invoices.billingreason)
            over (partition by invoices.user_id order by invoices.paymentdate)
        = 'subscription_update', false)
        as is_updated,

        case
            when
                invoices.billingreason = 'subscription_update'
                then
                    case
                        when
                            invoices.subscription_type ilike '%Yearly%'
                            or invoices.subscription_type ilike '%Annual%'
                            or invoices.subscription_type ilike '%Yeaerly%'
                            or invoices.subscription_type ilike '%Edu (%'
                            then
                                dateadd(
                                    month,
                                    12,
                                    date_trunc(
                                        'day',
                                        invoices.createdat
--                                        lag(invoices.createdat)
--                                            over (
--                                                partition by invoices.user_id
--                                                order by invoices.createdat
--                                            )
                                    )
                                )
                        when
                            invoices.subscription_type not ilike '%yearly%'
                            and invoices.subscription_type not ilike '%annual%'
                            and invoices.subscription_type not ilike '%yeaerly%'
                            and ((invoices.subscription_type ilike 'Edu Medium' and invoices.original_amount::float > 12)
                                or (invoices.subscription_type ilike 'Edu Large' and invoices.original_amount::float > 25)
                                or (invoices.subscription_type ilike 'Edu Small' and invoices.original_amount::float > 6)
                                )
                            then
                                dateadd(
                                    month,
                                    12,
                                    date_trunc(
                                        'day',
                                        invoices.createdat
                                    )
                                )
                        else
                            dateadd(
                                month,
                                1,
                                date_trunc(
                                    'day',
                                    invoices.createdat
--                                    lag(invoices.createdat)
--                                        over (
--                                            partition by invoices.user_id
--                                            order by invoices.createdat
--                                        )
                                )
                            )
                    end
            else
                case
                    when invoices.subscription_type ilike '%One-Time%'
                        then date_trunc('day', invoices.createdat)
                    when
                        invoices.subscription_type ilike '%Yearly%'
                        or invoices.subscription_type ilike '%Annual%'
                        or invoices.subscription_type ilike '%Yeaerly%'
                        or invoices.subscription_type ilike '%Edu (%'
                        then
                            dateadd(
                                month,
                                12,
                                date_trunc(
                                    'day',
                                    invoices.createdat
--                                    lag(invoices.createdat)
--                                        over (
--                                            partition by invoices.user_id
--                                            order by invoices.createdat
--                                        )

                                )
                            )
                    when
                        invoices.subscription_type not ilike '%yearly%'
                        and invoices.subscription_type not ilike '%annual%'
                        and invoices.subscription_type not ilike '%yeaerly%'
                        and ((invoices.subscription_type ilike 'Edu Medium' and invoices.amount::float > 12)
                            or (invoices.subscription_type ilike 'Edu Large' and invoices.amount::float > 25)
                            or (invoices.subscription_type ilike 'Edu Small' and invoices.amount::float > 6)
                            )
                            then
                                dateadd(
                                    month,
                                     12,
                                     date_trunc(
                                     'day',
                                     invoices.createdat
                                     )
                                )
                    else
                        dateadd(
                            month,
                            1,
                            date_trunc(
                                'day',
                                invoices.createdat
                            )
                        )
                end
        end as subscription_end_date,

        case
            when invoices.subscription_type ilike '%Yearly%'
                or invoices.subscription_type ilike '%annual%'
                or subscription_type ilike '%Yeaerly%'
                or invoices.subscription_type ilike '%Edu (%'
                then 'yearly'
            when
                invoices.subscription_type not ilike '%yearly%'
                and invoices.subscription_type not ilike '%annual%'
                and invoices.subscription_type not ilike '%yeaerly%'
                and ((invoices.subscription_type ilike 'Edu Medium' and invoices.amount::float > 12)
                    or (invoices.subscription_type ilike 'Edu Large' and invoices.amount::float > 25)
                    or (invoices.subscription_type ilike 'Edu Small' and invoices.amount::float > 6)
                    )
                    then 'yearly'
            when invoices.subscription_type ilike '%One-Time%' then 'one_time'
            when invoices.subscription_type ilike '%White Label%' then 'White Label'
            else 'monthly'
        end as subscription_group,

        case
            when invoices.subscription_type ilike '%essential%' then 'Essential'
            when invoices.subscription_type ilike '%plus%' then 'Plus'
            when invoices.subscription_type ilike '%pro%' then 'Pro'
            when invoices.subscription_type ilike '%large%'
                and invoices.subscription_type ilike '%edu%'
                then 'Edu_large'
            when invoices.subscription_type ilike '%medium%'
                and invoices.subscription_type ilike '%edu%'
                then 'Edu_med'
            when invoices.subscription_type ilike '%small%'
                and invoices.subscription_type ilike '%edu%'
                then 'Edu_small'
            when invoices.subscription_type ilike '%one-time%' then 'OneTime'
            else 'others'
        end as plan_type,
--
--        case
--            when
--                invoices.subscription_type ilike '%Yearly%' or invoices.subscription_type ilike '%annual%'
--                then 365
--            when invoices.subscription_type ilike '%One-Time%'
--                then 1
--            else 30
--        end as subscription_duration_day,

        row_number()
            over (
                partition by invoices.user_id order by invoices.paymentdate desc
            )
        as order_ranking_desc,
        row_number()
            over (
                partition by invoices.user_id order by invoices.paymentdate asc
            )
        as order_ranking_asc
    from invoices
        left join internal_user on invoices.user_id = internal_user.user_id
    where is_b2b_on_stripe is false
        and internal_user.user_id is null
)

select * from int_invoices
--where actual_paid > 0
