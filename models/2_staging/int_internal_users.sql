
{{ config(materialized='table', unique_key='user_id', tags = ["staging"],
pre_hook="set statement_timeout to 120000") }}
with
users as (select distinct
    user_id,
    email,
    domain
    from {{ ref('base_users') }}
),

internal_user as (
    select user_id
    from users
    where
        email::text ~~ '%khanhlinh24294%'::text
        or email::text ~~ '%huongnguyen2152@gmail.com%'::text
        or email::text ~~ 'linhlinh7490@gmail.com'::text
        or email::text ~~ 'minhtrangnguyen1612@gmail.com'::text
        or email::text ~~ 'gianghn@gmail.com'::text
        or email::text ~~ '%@ahaslides.com%'::text
        or email::text ~~ '%lindo2510@gmail.com%'::text
        or email::text ~~ '%@v.vn%'::text
        or email::text ~~ '%@a.com%'::text
        or email::text ~~ '%stove%'::text
        or email::text ~~ '%bunnycry%'::text
        or email::text ~~ '%dmtrinh%'::text
        or email::text ~~ '%chloe+%'::text
        or email::text ~~ '%+test@%'::text
        or email::text ~~ '%vuthuyduong.sf@gmail.com%'::text
        or email::text ~~ '%eafaef@feaafaef.com%'::text
        or email::text ~~ '%mrdungx@gmail.com%'::text
        or email::text ~~ '%hai@xxx.com%'::text
        or email::text ~~ '%tduc94@%'::text
        or email::text ~~ '%tduc94+%'::text
        or email::text ~~ '%@manhtai%'::text
        or email::text ~~ '%mindthegapstudio%'::text
        or email::text ~~ '%test%'::text
        or email::text ~~ '%test+%'::text
        or email::text ~~ '%@qa.com%'::text
        or email::text ~~ '%lawrencehaywood%'::text
        or email::text ~~ '%vomanhtai%'::text
)

select * from internal_user