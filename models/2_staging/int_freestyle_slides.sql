{{ config(materialized='table', unique_key='slide_id', tags = ["staging"]) }}
with slides as (
    select * from {{ ref('base_slides') }}
),

freestyle as (
    select
        presentation_id,
        slide_id,
        base_type,
        json_parse(lower(metadata)) as metadata_parse
    from slides
    where base_type in ('document', 'freestyle')

)

select
    presentation_id,
    slide_id,
    base_type,
    metadata_parse.isimportedfromppt::boolean as is_imported_slides
from freestyle
