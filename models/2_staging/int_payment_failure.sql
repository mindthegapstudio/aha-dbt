{{ config(materialized='table', sort='createdat', unique_key='payment_failure_id', tags = ["staging"],
pre_hook="set statement_timeout to 120000") }}
with user_event as (
    select * from {{ ref('base_user_history') }}
),

payment_failure_raw as (
    select
        history_id as payment_failure_id,
        user_id,
        createdat,
        updatedat,
        JSON_PARSE(LOWER(meta)) as error_details
    from user_event
    where
        1 = 1
        and user_action = 'Pay failed'
),

payment_failure_processed as (
    select
        payment_failure_id,
        user_id,
        error_details.plan as plan_selected,
        updatedat,
        error_details,
        error_details.planprice as failure_transaction_amount,
        error_details.paymentsource as payment_source,
        DATE_TRUNC('day', createdat) as createdat,
        JSON_PARSE(CAST(error_details.log as varchar)) as log_details,
        JSON_PARSE(CAST(error_details.error as varchar)) as error_code
    from payment_failure_raw
)

select * from payment_failure_processed
