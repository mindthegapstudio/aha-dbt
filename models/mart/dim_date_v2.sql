
{{ config(materialized='table', 
sort='date_key', 
unique_key='date_key', 
tags = ["mart"]) }}

with RECURSIVE date_series(date_key) as ( -- 23 years from 2017
    SELECT CAST('2040-01-01' AS DATE) AS date_key
    UNION ALL
    SELECT CAST(date_key - INTERVAL '1 day' AS DATE)
    FROM date_series
    WHERE date_key > '2017-01-01' -- 23 years from 2017
)

select 
	date_key,
	TO_CHAR(date_key::date, 'Mon DD, YYYY') as date_char,
	cast(date_part('year', date_key) as int) as year_,
	cast(date_part('month', date_key) as int) as  month_,
	cast(date_part('day', date_key) as int) as  day_in_month,
	date_part('dow', date_key) as day_of_week_number,
	case when date_part('dow', date_key) = 0 then 'SUN'
		 when date_part('dow', date_key) = 1 then 'MON'
		 when date_part('dow', date_key) = 2 then 'TUE'
		 when date_part('dow', date_key) = 3 then 'WED'
		 when date_part('dow', date_key) = 4 then 'THU'
		 when date_part('dow', date_key) = 5 then 'FRI'
		 when date_part('dow', date_key) = 6 then 'SAT'
		 end AS day_of_week_name,
	case when date_part('dow', date_key) = 0 then 'WEEKEND'
		 when date_part('dow', date_key) = 1 then 'WEEKDAY'
		 when date_part('dow', date_key) = 2 then 'WEEKDAY'
		 when date_part('dow', date_key) = 3 then 'WEEKDAY'
		 when date_part('dow', date_key) = 4 then 'WEEKDAY'
		 when date_part('dow', date_key) = 5 then 'WEEKDAY'
		 when date_part('dow', date_key) = 6 then 'WEEKEND'
		 end AS daytype_in_week
from date_series