
{{ config(materialized='table', unique_key='presentation_id', tags = ["mart"], schema='data_mart',
pre_hook="set statement_timeout to 120000") }}
with presentation as (
    select * from {{ ref('int_presentations') }}
    where
        1 = 1
        and online_count >= 4
        and online_count_time is not null
),


fct_presentation as (
    select
        presentation.presentation_id,
        presentation.presentation_name,
        presentation.presentation_created_at,
        presentation.presentation_owner_id,
        presentation.is_deleted,
        presentation.deleted_at,
        presentation.online_count_time,
        presentation.online_count,
        presentation.audio_name,
        presentation.is_chat,
        presentation.team_id,
        lag(online_count_time)
            over (
                partition by
                    presentation_owner_id
                order by presentation.online_count_time
            )
        as last_event_time,
        lead(online_count_time)
            over (
                partition by
                    presentation_owner_id
                order by online_count_time
            )
        as next_event_time
    from presentation

)

select
    *,
    coalesce(
        date_diff('day', online_count_time, next_event_time) <= 28
        and date_diff('day', online_count_time, next_event_time) >= 1,
        false
    ) as is_retended
from fct_presentation
