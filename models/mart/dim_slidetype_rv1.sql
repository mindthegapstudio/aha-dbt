{{ config(materialized='table',
schema='data_mart') }}

with table_raw as (
select distinct 
	"type",
	"slidetype", 
	case when "metadata" like '%isImportedFromPPT%' then json_extract_path_text("metadata", 'isImportedFromPPT') end AS is_imported_from_ppt  
from "rds-default-production-aha"."public_slides"
where 1=1
--and lower(type) = 'document'
--and lower(metadata) like '%isimportedfromppt%'
) 

, table_slidetype_name as (
select distinct 
    "type",
    "slidetype" as "slidetype_raw",
case when lower("type") = 'multiplechoice' then 'Poll'
     when lower("type") = 'imagechoice' then 'Poll'
     when lower("type") = 'openended' then 'Open Ended'
     when lower("type") = 'wordcloud' then 'Word Cloud'
     when lower("type") = 'scale' then 'Scales'
     when lower("type") = 'ideas' then 'Brainstorm'
     when lower("type") = 'question' then 'Q&A'
     when lower("type") = 'pickanswer' and lower(slidetype) = 'imagechoice' then 'Pick Image'
     when lower("type") = 'pickanswer' and lower(slidetype) = 'typeanswer' then 'Short Answer'
     when lower("type") = 'pickanswer' and lower(slidetype) = 'correctorder' then 'Correct Order'
     when lower("type") = 'pickanswer' and lower(slidetype) = 'matchpairs' then 'Match Pairs'
     when lower("type") = 'pickanswer' and lower(slidetype) is null then 'Pick Answer'
     when lower("type") = 'leaderboard' then 'Quiz Leaderboard'
     when lower("type") = 'textslide' then 'Heading'
     when lower("type") = 'bulletpoints' then 'List'
     when lower("type") = 'qrcode' then 'QR code'
     when lower("type") = 'image' then 'Image'
     when lower("type") = 'youtube' then 'Youtube'
     when lower("type") = 'googleslides' then 'Google Slides'
     when lower("type") = 'document' then 'Document'
     when lower("type") = 'freestyle' and is_imported_from_ppt = 'true' then 'Document'
     when lower("type") = 'freestyle' and is_imported_from_ppt is null then 'Content'
     when lower("type") = 'spinnerwheel' then 'Spinner Wheel'
     end as "slidetype_name"
from table_raw
)


select
    row_number() over () as slidetype_id,
    *
from table_slidetype_name
order by "type","slidetype_raw"