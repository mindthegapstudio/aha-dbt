
{{ config(materialized='table',  unique_key='session_id', tags = ["mart"], schema='data_mart') }}
--psql --echo-all --host=your_host --port=your_port --dbname=your_dbname --username=your_username --sslmode=require -f your_file.sql
with first_events as (
    select presenter_id, presentation_id, min(start_time) as first_event_time
    from {{ ref('int_events') }}
    where online_count >=4
    {% if is_incremental() %}
    and updated_at >= (select date_add('day', -5, current_date))
    {% endif %}
    group by 1, 2
),

events as (
    select *,
--        first_value(start_time)
--                over (
--                    partition by
--                        presenter_id, presentation_id
--                    order by
--                        start_time
--                    rows between unbounded preceding and unbounded following
--                )
--        as first_event_time,

--        first_value(session_id)
--                over (
--                    partition by
--                        presenter_id, presentation_id
--                    order by
--                        start_time
--                    rows between unbounded preceding and unbounded following
--                )
--        as first_session_id,

        lag(start_time)
                over (
                    partition by
                        presenter_id, presentation_id
                    order by start_time
                )
        as last_event_time,

        lag(start_time)
                over (
                    partition by
                        presenter_id
                    order by start_time
                )
        as last_live_event_time,

        lag(session_id)
                over (
                    partition by
                        presenter_id, presentation_id
                    order by start_time
                )
        as last_session_id,

        datediff(
            hour,
            lag(start_time)
                over (
                    partition by
                        presenter_id, presentation_id
                    order by start_time
                ),
            start_time
        )
        as time_from_previous_event_new,

        lead(start_time)
                over (
                    partition by
                        presenter_id, presentation_id
                    order by start_time
                    )
        as next_event_time,

        lead(session_id)
                over (
                    partition by
                        presenter_id, presentation_id
                    order by start_time
                    )
        as next_session_id,

        datediff(
            hour,
            start_time,
            lead(start_time)
                over (
                    partition by
                        presenter_id, presentation_id
                    order by start_time
                    )
        )
        as time_to_next_event_new,

        datediff(
            hour,
            lag(start_time)
                over (
                    partition by
                        presenter_id, presentation_id
                    order by start_time
                ),
            lead(start_time)
                over (
                    partition by
                        presenter_id, presentation_id
                    order by start_time
                    )
        )
        as time_from_last_event_to_next_event_new

--        row_number()
--            over (
--                partition by presenter_id, presentation_id, trunc(start_time)
--                order by start_time asc
--            )
--        as same_event_row_new
    from {{ ref('int_events') }}
    where online_count >=4
    {% if is_incremental() %}
    and updated_at >= (select date_add('day', -5, current_date))
    {% endif %}
),

base_events as (
    select
        events.*,
        first_events.first_event_time
    from events
        inner join first_events
            on events.presenter_id = first_events.presenter_id
                and events.presentation_id = first_events.presentation_id
),

users as (
    select * from {{ ref('int_users') }}
),

slides as (
    select * from {{ ref('int_slides_final') }}
),
--
--
slides_total as (
    select
        presentation_id,
        bool_or(coalesce(is_ai_slides)) as is_used_ai
    from slides
    group by 1
),

raw_events as (
    select
    case
        when start_time = first_event_time then session_id
        when start_time <> first_event_time and last_event_time = first_event_time and time_from_previous_event_new >= 3 then session_id
        when start_time <> first_event_time and last_event_time = first_event_time and next_session_id is not null and time_from_previous_event_new < 3 and time_to_next_event_new >= 3 then next_session_id
        when start_time <> first_event_time and last_event_time = first_event_time and next_session_id is not null and time_from_previous_event_new < 3 and time_to_next_event_new < 3 then null
        when start_time <> first_event_time and last_event_time = first_event_time and next_session_id is null and time_from_previous_event_new < 3 then null
        when start_time <> first_event_time and last_event_time <> first_event_time and time_from_previous_event_new >= 3 then session_id
        when start_time <> first_event_time and last_event_time <> first_event_time and next_session_id is null and time_from_previous_event_new < 3 and time_from_last_event_to_next_event_new >= 3 then next_session_id
        when start_time <> first_event_time and last_event_time <> first_event_time and next_session_id is not null and time_from_previous_event_new < 3 and time_from_last_event_to_next_event_new < 3 then null
        when start_time <> first_event_time and last_event_time <> first_event_time and next_session_id is null and time_from_previous_event_new < 3 then null
    end as event_id
    from base_events
    ),

total_events as (
    select
        raw_events.event_id, 
        case
            when raw_events.event_id is not null then true
            else false
        end as is_events,
        base_events.session_id,
        base_events.presentation_id,
        case when base_events.source is not null then base_events.source
             when base_events.source is null then 'ahaslides presenter app'
             end as source,
        base_events.presenter_id,
        base_events.start_time as start_time, 
        -- CONVERT_TIMEZONE('Asia/Ho_Chi_Minh',base_events.start_time) as start_time_utc7,
        base_events.online_count,
        base_events.participant_count,
        base_events.user_subscription,
        users.use_ahaslides_for,
        users.is_internal,
        users.is_hacker,
        slides_total.is_used_ai,
        base_events.font_family,
        base_events.slide_count,
        base_events.is_qna_all_slide::boolean,
        base_events.is_enable_chat::boolean,
        base_events.is_anonymous_qna::boolean,
        base_events.is_show_hyperlink::boolean,
        base_events.is_audience_pacing::boolean,
        base_events.number_of_authentication_code_generated,
        base_events.team_scoring_rule,
        base_events.is_moderated_audience_question::boolean,
        base_events.is_show_ahaslides_cta::boolean,
        base_events.is_enable_quiz_music::boolean,
        base_events.is_reaction_enabled::boolean,
        base_events.is_show_ahaslides_logo::boolean,
        base_events.is_quiz_countdown_enabled::boolean,
        base_events.is_filtering_profanity::boolean,
        base_events.is_hide_instruction_bar::boolean,
        base_events.is_qna_audience_show_all::boolean,
        base_events.is_audience_authentication::boolean,
        base_events.is_enable_copy_slide_note::boolean,
        base_events.is_has_example_responses::boolean,
--        row_number()
--            over (
--                partition by presenter_id order by start_time
--            )
--        as event_ranking_asc,
        base_events.last_event_time,
        base_events.last_live_event_time,
        case
            when (
                users.team_id is not null
                and users.team_plan_endate > base_events.start_time
            )
                then users.team_subscription
            else base_events.user_subscription
        end as hosting_subscription
    from base_events
    left join users
        on base_events.presenter_id = users.user_id
    left join slides_total
        on base_events.presentation_id = slides_total.presentation_id
    left join raw_events
        on base_events.session_id = raw_events.event_id
),

fct_events as (select distinct
    *,
    case
        when date_diff('day', last_live_event_time, start_time) is null then 'virgin'
        when date_diff('day', last_live_event_time, start_time) <= 28 then 'retention'
        when date_diff('day', last_live_event_time, start_time) > 28 then 'resurrection'
    end as retention_status
from total_events
where
    is_hacker is false
    and is_internal is false
    and event_id is not null
)

select * from fct_events