{{ config(schema='data_mart', tags=["mart"]) }}
with jira_sprints as (
    select * from {{ ref('base_jira_sprints') }}
),

velocity as (
    select * from {{ ref('base_jira_velocity') }}
)

select
    velocity.sprint_id,
    velocity.board_id,
    jira_sprints.sprint_name,
    jira_sprints.start_date,
    jira_sprints.end_date as projected_end_date,
    jira_sprints.created_date,
    jira_sprints.complete_date,
    velocity.estimated_points,
    velocity.completed_points,
    case
        when velocity.board_id = 1 then 'AHA'
        when velocity.board_id = 7 then 'ADA'
        when velocity.board_id = 8 then 'ALAN'
    end as team_name


from velocity left join jira_sprints
    on
        velocity.sprint_id = jira_sprints.sprint_id
        and velocity.board_id = jira_sprints.board_id
