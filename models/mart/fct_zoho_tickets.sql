{{ config(schema='data_mart') }}

with tickets as (
    select * from {{ ref('base_zoho_tickets') }}
),

threads as (
    select distinct 
        ticket_id, 
        channel,
        first_value( author__email ) over (partition by ticket_id, channel order by created_time rows between unbounded preceding and unbounded following) as first_response_agent_email,
        first_value( author__name ) over (partition by ticket_id, channel order by created_time rows between unbounded preceding and unbounded following) as first_response_agent_name,
        first_value(created_time) over (partition by ticket_id, channel order by created_time rows between unbounded preceding and unbounded following) as first_response_time
    from {{ ref('base_zoho_threads') }}
    where author__type = 'AGENT'

),

ratings as (
    select * from {{ ref('base_zoho_reviews') }}
),

dedup as (
select 
    tickets.ticket_id,
    tickets.channel,
    tickets.subject,
    tickets.contact__email,
    tickets.contact__is_spam,
    tickets.created_time,
    tickets.assignee__email as current_assignee_email,
    tickets.custom_fields__label as label ,
    ratings.feedback,
    ratings.rated_time,
    ratings.rating,
    threads.first_response_agent_name,
    threads.first_response_agent_email,
    threads.first_response_time,
    rank()over(partition by tickets.ticket_id order by  ratings.rated_time desc,  threads.first_response_time ) as rank
from tickets left join threads
    on tickets.ticket_id = threads.ticket_id
left join ratings
    on tickets.ticket_id = ratings.ticket_id
)

select * from dedup 
where rank = 1

