{{ config(materialized='table', sort='month_series', dist='month_series', tags = ["mart"], schema='data_mart',
pre_hook="set statement_timeout to 120000") }}

with invoices as (
    select
        *
    from {{ ref('int_raw_mrr_b2c') }}
),

date_spine as (
    select
        date_trunc('day', date) as date_series
    from {{ ref('dim_date') }}
    where date >= cast('2019-07-01' as date)
      and date < dateadd('year', 1, current_date)
),

aggregated_data_prosumer as (
    select
        date_series,
        last_value(date_series) over (
            partition by date_trunc('month', date_series)
            order by date_series
            rows between unbounded preceding and unbounded following
        ) as last_day_of_month,
        sum(case
                when date_spine.date_series >= invoices.plan_start
                    and date_spine.date_series < invoices.plan_end
                    then mrr
                else null
            end) as total_mrr,
--        sum(case
--                when subscription_group = 'one_time'
--                    and date_spine.date_series >= invoices.plan_start
--                    and date_spine.date_series < invoices.plan_end
--                    then mrr
--                else null
--            end) as total_mrr_onetime,
        sum(case
                when date_spine.date_series = invoices.plan_start
                    and invoices.category = 'new'
                    then mrr
                else 0
            end) as new_mrr,
        sum(case
                when date_spine.date_series = invoices.plan_start
                    and invoices.category = 'expansion'
                    then mrr
                else 0
            end) as expansion_mrr,
        sum(case
                when date_spine.date_series = invoices.plan_start
                    and date_spine.date_series < invoices.plan_end
                    and category = 'recurring'
                    then mrr
                else 0
            end) as recurring_mrr
    from date_spine
        left join invoices
            on date_spine.date_series >= invoices.plan_start
            and date_spine.date_series < invoices.plan_end
    where subscription_group <> 'one_time'
    group by 1
),

aggregated_data_onetime as (
    select
        date_trunc('month', date_series) as month_series,
        sum(mrr) as total_mrr
    from date_spine
        left join invoices
            on date_spine.date_series = invoices.plan_start
    where subscription_group = 'one_time'
    group by 1
),


month_prosumer_total as (
    select
        date_trunc('month', date_series) as month_series,
        total_mrr
    from aggregated_data_prosumer
    where date_series = last_day_of_month
),

month_prosumer_other as (
    select
        date_trunc('month', date_series) as month_series,
        sum(new_mrr) as new_mrr,
        sum(expansion_mrr) as expansion_mrr,
        sum(recurring_mrr) as recurring_mrr
    from aggregated_data_prosumer
    group by 1
),

--month_aggregated_data as (
--    select
--        p.month_series
--        last_day_of_month,
--        max(total_mrr) as total_mrr,
--        max(total_mrr_prosumer) as total_mrr_prosumer,
--        sum(total_mrr_onetime) as total_mrr_onetime,
--        sum(new_mrr) as new_mrr,
--        sum(expansion_mrr) as expansion_mrr,
--        sum(recurring_mrr) as recurring_mrr
--    from aggregated_data
--    group by 1
--),

lead_data_prosumer as (
    select
        total.month_series,
        total.total_mrr,
        lead(total.total_mrr, 1) over (order by total.month_series desc) as previous_total_mrr,
        other.new_mrr,
        other.expansion_mrr
    from month_prosumer_total as total
        left join month_prosumer_other as other
            on total.month_series = other.month_series
),

--lead_data_onetime as (
--    select
--        month_series,
--        total_mrr
--    from aggregated_data_onetime
--),

prosumer_mrr as (
    select
        month_series,
        total_mrr,
        total_mrr - previous_total_mrr as net_new_mrr,
        new_mrr,
        expansion_mrr,
        total_mrr - new_mrr - expansion_mrr as recurring_mrr,
        total_mrr - previous_total_mrr - new_mrr - expansion_mrr as churn_mrr
    from lead_data_prosumer
),

onetime_mrr as (
    select
        month_series,
        total_mrr
    from aggregated_data_onetime
),

fct_mrr as (
    select
        prosumer.month_series,
        coalesce(prosumer.total_mrr,0) + coalesce(onetime.total_mrr,0) as total_mrr,
        coalesce(prosumer.total_mrr,0) as prosumer_mrr,
        coalesce(onetime.total_mrr,0) as one_time_mrr,
        coalesce(prosumer.net_new_mrr,0) as prosumer_net_new,
        coalesce(prosumer.new_mrr,0) as prosumer_new_mrr,
        coalesce(prosumer.expansion_mrr,0) as prosumer_expansion_mrr,
        coalesce(prosumer.recurring_mrr,0) as prosumer_recurring_mrr,
        coalesce(prosumer.churn_mrr,0) as prosumer_churn_mrr
    from prosumer_mrr as prosumer
        left join onetime_mrr as onetime
            on prosumer.month_series = onetime.month_series
)

select * from fct_mrr

--with invoices as (
--    select
--        *,
--        date_trunc('month', payment_date) as payment_month,
--        case
--            when billing_reason = 'subscription_update' then 'expansion'
--            when billing_reason = 'subscription_cycle' then 'recurring'
--            else 'new'
--        end as category
--    from {{ ref('int_invoices_b2c') }}
--    where
--        actual_paid > 0
--),
--
--
--date_spine as (
--    select
--        date_trunc('month', date) as date
--    from {{ ref('dim_date') }}
--    where date >= cast('2019-10-01' as date)
--      and date < dateadd('year', 1, current_date)
--    group by date_trunc('month', date)
--),
--
--yearly_sub as (
--    select
--        payment_month,
--        invoice_id,
--        user_id,
--        category,
--        subscription_group,
--        subscription_plan,
--        date_trunc('month', subscription_end_date) as subscription_end_month,
--        actual_paid / 12 as monthly_paid
--    from invoices
--    where subscription_group = 'yearly'
--),
--
--
--mrr_yearly as (
--    select
--        {{ dbt_utils.generate_surrogate_key(['invoice_id', 'date_month']) }} as monthly_rev_id,
--        user_id,
--        date_spine.date_month as payment_month,
--        yearly_sub.category,
--        yearly_sub.subscription_group,
--        yearly_sub.subscription_plan,
--        yearly_sub.monthly_paid
--    from date_spine
--    left join yearly_sub
--        on
--            date_spine.date_month >= yearly_sub.payment_month
--            and date_spine.date_month < yearly_sub.subscription_end_month
--),
--
--mrr_monthly as (
--    select
--        {{ dbt_utils.generate_surrogate_key(['invoice_id', 'payment_month']) }} as monthly_rev_id,
--        user_id,
--        payment_month,
--        category,
--        subscription_group,
--        subscription_plan,
--        actual_paid
--    from invoices
--    where subscription_group != 'yearly'
--
--)
--
--
--select * from mrr_yearly
--union all
--select * from mrr_monthly
