
{{ config(materialized='table', sort='traffic_date', dist='traffic_date', tags = ['mart'], schema='data_mart',
pre_hook="set statement_timeout to 120000") }}
with ga4_sessions as (
    select * from {{ ref('base_ga4_session_new') }}
),

events as (
    select * from {{ ref('base_ga4_events_new') }}
    where event_name = 'sign_up'
),

purchase as (
    select * from {{ ref('base_ga4_events_new') }}
    where event_name = 'purchase'
),

signup_event as (
    select
        event_date::date,
        country,
        sum(event_count_integer) as total_event
    from events
    where 1 = 1
    group by 1, 2
),

purchase_event as (
    select
        event_date::date,
        country,
        sum(event_count_integer) as total_purchase
    from purchase
    where 1 = 1
    group by 1, 2
),

traffic as (
    select
        traffic_date,
        country,
        sum(sessions_integer) as total_traffic
    from ga4_sessions
    where 1 = 1
    group by 1, 2

),

total as (
    select
        traffic.traffic_date,
        traffic.total_traffic,
        traffic.country,
        purchase_event.total_purchase,
        signup_event.total_event,
        dateadd(year, -1, traffic.traffic_date) as last_year
    from traffic left join signup_event
        on
            traffic.traffic_date = signup_event.event_date
            and traffic.country = signup_event.country
    left join purchase_event
        on
            traffic.traffic_date = purchase_event.event_date
            and traffic.country = purchase_event.country

)

select
    total.traffic_date,
    total.total_traffic,
    total.total_event,
    total.country,
    total.total_purchase,
    yoy.total_traffic as yoy_total_traffic,
    yoy.total_event as yoy_total_event,
    yoy.total_purchase as yoy_total_purchase
from total left join total as yoy on
    total.last_year = yoy.traffic_date
    and total.country = yoy.country
