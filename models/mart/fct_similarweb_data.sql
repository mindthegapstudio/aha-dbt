{{ config(schema='data_mart', tags=["mart"]) }}
with all_source as (
    select * from {{ ref('base_similarweb_ahaslides') }}
    union all
    select * from {{ ref('base_similarweb_classpoint') }}
    union all
    select * from {{ ref('base_similarweb_kahoot') }}
    union all
    select * from {{ ref('base_similarweb_mentimeter') }}
    union all
    select * from {{ ref('base_similarweb_pollev') }}
    union all
    select * from {{ ref('base_similarweb_quizziz') }}
    union all
    select * from {{ ref('base_similarweb_slido') }}
)


select * from all_source
