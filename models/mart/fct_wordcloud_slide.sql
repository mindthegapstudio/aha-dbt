{{ config(schema='data_mart') }}

with slides as (
    select * from {{ ref('base_slides') }}
    where
        slide_type = 'wordCloud'
        and created_at >= date('2024-01-01')
),

slides_options as (
    select * from {{ ref('base_slide_options') }}
)

select
    slides.slide_id,
    slides.created_at,
    slides.presentation_id,
    slides.wordcloudsmartgrouping,
    slides.isgroupwordcloudwords,
    slides.numberofwordsingroup,
    slides.lasttimegroupwordcloud,
    slides.numberofuniquewordsingroup,
    listagg(slides_options.title, ',') as worldcloud_input
from slides left join slides_options
    on
        slides.slide_id = slides_options.slide_id
group by 1, 2, 3, 4, 5, 6, 7, 8
