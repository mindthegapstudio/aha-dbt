{{ config(materialized='table',
unique_key='presentation_id',
schema='data_mart',
tags = ["mart"]) }}
with presentation as (
    select * from {{ ref('int_presentations') }}
),

imported_slides as (
    select distinct presentation_id
    from {{ ref('int_freestyle_slides') }}
    where
        base_type = 'document'
        or is_imported_slides is true
),

ratings as (
    select
        presentation_id,
        count(distinct rating_id) as amount_of_ratings,
        count(distinct rater_id) as amount_of_raters
    from {{ ref('base_ratings') }}
    group by 1
),

comments as (
    select
        presentation_id,
        count(distinct comment_id) as amount_of_comments,
        count(distinct commenter_id) as ammount_of_commenters
    from {{ ref('base_comments') }}
    group by 1
),

interaction_rate as (
    select *

    from {{ ref('int_interaction_rate') }}

),

invited_user as (
    select
        presentation_id,
        count(
            distinct case when is_owner is false then user_id end
        ) as total_user_invited,
        count(
            distinct case
                when is_owner is false and is_paid is false then user_id
            end
        ) as free_user_invited,
        count(
            distinct case
                when is_owner is false and is_paid is true then user_id
            end
        ) as paid_user_invited
    from {{ ref('fct_user_invitation') }}
    group by 1
),

slides as (
    select * from {{ ref('int_slides_final') }}
),


slides_total as (
    select
        presentation_id,
        bool_or(COALESCE(is_ai_slides, false)) AS is_used_ai
    from slides
    group by 1
),

fct_presentation as (
    select
        presentation.presentation_id,
        presentation.presentation_name,
        presentation.presentation_created_at,
        presentation.presentation_owner_id,
        presentation.is_deleted,
        presentation.deleted_at,
        presentation.online_count_time,
        presentation.online_count,
        presentation.is_team_play,
        presentation.is_audience_pacing,
        presentation.is_hide_individual_leaderboard,
        presentation.is_quiz_music,
        presentation.is_sample_responses,
        presentation.updated_at,
        presentation.is_private_feedback,
        presentation.numberoflikes,
        presentation.numberofhearts,
        presentation.numberoflaughs,
        presentation.numberofsads,
        presentation.numberofwows,
        presentation.is_streak_bonus,
        presentation.audio_name,
        presentation.is_chat,
        presentation.team_id,
        presentation.is_created_from_template,
        presentation.is_created_from_community_template,
        presentation.is_created_from_other_presentation,
        presentation.thumbnail_type,
        presentation.bulletpoints_count,
        presentation.correctorder_count,
        presentation.document_count,
        presentation.freestyle_count,
        presentation.googleslides_count,
        presentation.ideas_count,
        presentation.image_count,
        presentation.imagechoice_count,
        presentation.leaderboard_count,
        presentation.matchpairs_count,
        presentation.multiplechoice_count,
        presentation.openended_count,
        presentation.pickanswer_count,
        presentation.qrcode_count,
        presentation.question_count,
        presentation.scale_count,
        presentation.spinnerwheel_count,
        presentation.textslide_count,
        presentation.typeanswer_count,
        presentation.wordcloud_count,
        presentation.youtube_count,
        presentation.total_slides,
        comments.amount_of_comments,
        comments.ammount_of_commenters,
        ratings.amount_of_ratings,
        ratings.amount_of_raters,
        interaction_rate.interacted_audience,
        interaction_rate.total_audience,
        invited_user.free_user_invited,
        invited_user.paid_user_invited,
        invited_user.total_user_invited,
        is_thumbnail_uploaded,
        is_thumbnail_changed,
        interaction_rate.interacted_audience::float
        / nullif(total_audience, 0)::float as interaction_rate,
        row_number()
            over (
                partition by presentation_owner_id
                order by online_count_time desc
            )
        as presentation_ranking_desc,
        row_number()
            over (
                partition by presentation_owner_id
                order by online_count_time asc
            )
        as presentation_ranking_asc,
        not coalesce(lower(font_family) = 'nunito', false) as is_changed_font,
        coalesce(
            imported_slides.presentation_id is not null, false
        ) as is_imported_slides,
--        coalesce(
--            invited_user.presentation_id is not null, false
--        ) as is_invited_user,
        slides_total.is_used_ai
    from presentation
    left join imported_slides
        on presentation.presentation_id = imported_slides.presentation_id
    left join invited_user
        on presentation.presentation_id = invited_user.presentation_id
    left join ratings
        on presentation.presentation_id = ratings.presentation_id
    left join comments
        on presentation.presentation_id = comments.presentation_id
    left join interaction_rate
        on presentation.presentation_id = interaction_rate.presentation_id
    left join slides_total
        on presentation.presentation_id = slides_total.presentation_id

)

select * from fct_presentation
