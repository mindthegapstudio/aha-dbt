
{{ config(materialized='table', sort='payment_date',
unique_key='invoice_id',
dist='invoice_id',
tags=["mart"],
schema='data_mart')}}

---- NOT EXCLUDE ACTUAL_AMOUNT <= 0

with int_invoices as (
    select * from {{ ref('int_invoices_b2c') }}
--    where
--        actual_paid > 0

),

fct_invoices as (select
    int_invoices.user_id,
    invoice_id,
    quantity,
--    quantity::float,
    payment_status,
    subscription_plan,
    amount_paid,
    amount_refunded,
    actual_paid,
    currency,
    exchangerate,
    billing_reason,
    plan_type,
    payment_source,
    payment_method,
    subscription_group,
    payment_date,
    payment_date_raw,
    coupon_code,
--    subscription_duration_day,
    subscription_end_date,
    order_ranking_asc,
    order_ranking_desc,
    is_updated,
    is_bulk,
    case
        when order_ranking_asc = 1 then 'new_order'
        when
            order_ranking_asc > 1 and billing_reason = 'subscription_cycle'
            then 'renewal'
        when
            order_ranking_asc > 1
            and billing_reason in ('manual', 'subscription_create')
            then 'revived'
        when
            order_ranking_asc > 1 and billing_reason = 'subscription_update'
            then 'update'
    end as current_retention_status,
    case
        when next_billing_reason = 'subscription_cycle' then 'renewal'
        when
            next_billing_reason in ('manual', 'subscription_create')
            then 'revived'
        when next_billing_reason = 'subscription_update' then 'upgrade'
        when
            next_billing_reason is null and subscription_end_date < current_date
            then 'churned'
        when
            next_billing_reason is null
            and subscription_end_date >= current_date
            then 'currently_subscribed'
    end as next_order_status
from int_invoices
-- where actual_paid > 0
)

, table_int_invoice as (
    select
    distinct
        invoice_id,
        amount_paid,
        user_id,
        payment_date_raw
    from {{ ref('int_invoices_b2c') }}
)

, table_signup as (
    select
    user_id,
    sign_up_date
    from {{ ref('int_users') }}
)

, table_usercampaign as (
    select
        userid as user_id,
        campaignid,
        "timestamp" as event_timestamp
    from {{ source('rds-default-production-aha', 'public_useradscampaigns') }}
) 

-- , table_campaign as (
--     select
--         *
--     from  from {{ source('rds-default-production-aha', 'public_adscampaigns') }}
-- )

, table_union as (
    select invoice_id, user_id, payment_date_raw as event_timestamp, null as campaignid, amount_paid
    from table_int_invoice
    union all
    select null as invoice_id, user_id, event_timestamp, campaignid, null as  amount_paid
    from table_usercampaign
)

, lag_time as (
    select 
        *, 
        lag(event_timestamp) over (partition by user_id order by event_timestamp) as lag_event_timestamp,
        lag(campaignid) over (partition by user_id order by event_timestamp) as lag_campaignid
    from table_union
)

, calculate_duration as (
        select
            *,
            case when lag_event_timestamp is not null and lag_campaignid is not null then date_diff('second', lag_event_timestamp, event_timestamp) end as duration
        from lag_time 
        where invoice_id is not null
)

, limit_time_duration as (
    select
        invoice_id,
        lag_campaignid,
        case when duration <= 864000 then lag_campaignid end as campaign_id
    from calculate_duration
)

, table_invoice_and_campaign as (
    select distinct
        invoice_id,
        campaign_id
    from limit_time_duration
)

select 
    a.*,
    b.campaign_id,
    CONVERT_TIMEZONE('Asia/Ho_Chi_Minh',a.payment_date_raw) as payment_date_raw_utc7,
    date(CONVERT_TIMEZONE('Asia/Ho_Chi_Minh',a.payment_date_raw)) as payment_date_utc7
    from fct_invoices a
left join table_invoice_and_campaign b
on a.invoice_id = b.invoice_id