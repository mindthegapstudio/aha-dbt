{{ config(materialized='table', sort='user_id', dist='createdat', tags = ["mart"], schema='data_mart',
pre_hook="set statement_timeout to 120000") }}
with
user_event as (
    select * from {{ ref('int_payment_failure') }}
)

select
    payment_failure_id,
    user_id,
    plan_selected,
    createdat,
    updatedat,
    failure_transaction_amount,
    COALESCE(
        error_code.payment_intent.last_payment_error.message,
        error_code."raw".message,
        error_code.message,
        log_details.type,
        payment_source
    ) as error_message
from user_event
