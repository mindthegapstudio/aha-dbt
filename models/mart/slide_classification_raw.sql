

with slides as (
    select * from {{ ref('base_slides') }}
),

slideoption as (
    select * from {{ ref('base_slide_options') }}
    where 1 = 1
    qualify
        row_number()
            over (
                partition by slide_id, presentationid order by len(title) desc
            )
        < 50
),

presentation as (
    select * from {{ ref('base_presentations') }}
),

user_c as (
    select
        user_id,
        user_country
    from {{ ref('dim_users') }}
)

select
    presentation.presentation_id,
    presentation.created_at,
    presentation.presentation_name,
    trim(slides.title) || (slides.subheading) as slide_content,
    listagg(trim(slideoption.title)) as slide_options
from presentation
left join slides on presentation.presentation_id = slides.presentation_id
left join slideoption on slides.slide_id = slideoption.slide_id
left join user_c on presentation.user_id = user_c.user_id
where user_country = 'Singapore'
-- left join question on question.presentation_id = presentation.presentation_id
-- left join comments on comments.presentation_id = presentation.presentation_id
-- left join private_feedback on private_feedback.presentation_id = presentation.presentation_id
group by 1, 2, 3, 4
