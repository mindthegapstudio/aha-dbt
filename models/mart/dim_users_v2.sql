{{ config(materialized='table', tags = ["mart"],
pre_hook="set statement_timeout to 120000",
schema='data_mart') }}
with
users as (
    select * from {{ ref('int_users') }}
    where
        1 = 1
        and is_hacker is false
),


country_region as (
    select * from {{ ref('country_region') }}
),


ld_report as (
    select * from {{ ref('base_ld_import') }}
),

user_invitation as (
    select
        inviter_id,
        count(distinct invitation_id) as total_invite
    from {{ ref('base_user_invitation') }}
    group by 1
),


dim_user as (
    select
        users.user_id,
        users.first_page,
        users.use_ahaslides_for,
        users.firstname,
        users.lastname,
        users.team_name,
        users.is_audience,
        users.user_email,
        users.sign_up_date,
        users.team_role,
        users.team_id,
        users.team_subscription,
        users.team_plan_endate,
        users.current_plan,
        users.user_country,
        country_region.region,
        -- coalesce( ld_report.user_id is not null, false) as is_ld_report,
        user_invitation.total_invite as total_invite_sent,
        users.user_city,
        -- ld_report.date_to_send_discount_code,
        users.is_internal,
        users.is_skoletube,
        users.is_audience_converted,
        users.account_creation_date,
        coalesce(
            user_invitation.inviter_id is not null, false
        ) as is_invited_user,
        coalesce(users.source is null, false) as is_organic
    from users
    left join country_region
        on users.user_country = country_region.name
    left join ld_report
        on users.user_id = ld_report.user_id
    left join user_invitation
        on users.user_id = user_invitation.inviter_id
)

select *
from dim_user
