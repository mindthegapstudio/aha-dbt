{{ config(materialized='table', sort='date_viewed', dist='date_viewed', tags = ["mart"], schema='data_mart') }}
WITH "users" AS(
SELECT
    u."id" AS "userId",
    CAST(p."onlineCountTime" AS TIMESTAMP) AS "eventHostedAt",
    -- (SELECT CAST(pr."onlineCountTime" AS TIMESTAMP) FROM "rds-default-production-aha"."public_presentations" AS pr WHERE pr."onlineCountTime" > p."onlineCountTime" LIMIT 1) AS "lastEventDate"
    LEAD(CAST(p."onlineCountTime" AS TIMESTAMP),1) OVER (PARTITION BY "userId" ORDER BY CAST(p."onlineCountTime" AS TIMESTAMP) DESC) AS "lastEventDate"
-- SELECT col1 FROM your_table t2 WHERE t2.order_column > t1.order_column LIMIT 1
FROM "rds-default-production-aha"."public_users" AS u
    LEFT JOIN "rds-default-production-aha"."public_presentations" AS p ON p."userId" = u."id"
    -- LEFT JOIN "rds-default-production-aha"."public_users_email" AS ue ON u."email" = ue."email"
    LEFT JOIN "rds-default-production-aha"."internal_users" AS iu ON u."id" = iu."id"
WHERE iu."id" IS NULL
AND "onlineCountTime" IS NOT NULL AND "onlineCount" >= 4
-- AND DATE(CAST(p."onlineCountTime" AS TIMESTAMP)) >= DATE('2021-01-01')
),

"raw" AS (
SELECT
    "userId",
    "eventHostedAt",
    "lastEventDate",
    DATEDIFF('day',"lastEventDate","eventHostedAt") AS "daySinceLastEvent"
FROM "users"
-- WHERE "userId" NOT IN (SELECT "id" FROM "rds-default-production-aha"."internal_users")
),

"rawNew" AS (
SELECT *
FROM "raw"
WHERE "lastEventDate" IS NULL
),

"rawReactivation" AS (
SELECT *
FROM "raw"
WHERE "daySinceLastEvent" >= 28
),

"timeseries" AS (SELECT DISTINCT
    DATE_TRUNC('day',CAST("createdAt" AS timestamp)) AS "date"
FROM "rds-default-production-aha"."public_users"
WHERE DATE(CAST("createdAt" AS TIMESTAMP)) >= DATE('2018-12-01')),

"RawTotal" AS (
SELECT 
    "timeseries"."date",
    "raw"."userId" as "allUser",
    "rawReactivation"."userId" as "reactivationUser",
    "rawNew"."userId" as "newUser"
FROM "timeseries"
    LEFT JOIN "raw" on "raw"."eventHostedAt" BETWEEN DATEADD('day',-28,"timeseries"."date") AND "timeseries"."date"
    LEFT JOIN "RawNew" on "RawNew"."eventHostedAt" = "raw"."eventHostedAt" AND "RawNew"."userid" = "raw"."userid"
    LEFT JOIN "RawReactivation" on "RawReactivation"."eventHostedAt" = "raw"."eventHostedAt" AND "RawReactivation"."userid" = "raw"."userid"
),


result AS (

    SELECT
        date AS date_viewed,
        DATE_ADD('year', -1, date) AS yoy_date,
        DATE_ADD('day', -28, date) AS previous_28_day,
     
        COUNT(DISTINCT alluser) AS "all",
        COUNT(DISTINCT newuser) AS "new",
        COUNT(reactivationuser) AS reactivation,

        COUNT(DISTINCT alluser)
        -
        COUNT(DISTINCT newuser)
        -
        COUNT(reactivationuser)
        AS returning
    FROM RawTotal
    GROUP BY 1
),

days_comparison as (

SELECT
    result.*,
    past_comparison.all as all_user_28_ago
    from  result
LEFT JOIN result as past_comparison     
    on result.previous_28_day = past_comparison.date_viewed
)

select 
   days_comparison.*,
   yoy_comparision.all AS yoy_all,
   yoy_comparision.all_user_28_ago AS yoy_all_user_28_ago,
   yoy_comparision.new AS yoy_new,
   yoy_comparision.reactivation AS yoy_reactivation,
   yoy_comparision.returning AS yoy_returning
FROM days_comparison
LEFT JOIN days_comparison AS yoy_comparision
    ON days_comparison.yoy_date = yoy_comparision.date_viewed