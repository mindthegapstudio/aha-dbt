{{ config(schema='data_mart', tags=["mart"]) }}

select
    slide_id,
    is_deleted,
    updated_at,
    created_at as created_at,
    -- CONVERT_TIMEZONE('Asia/Ho_Chi_Minh', created_at) as created_at_utc7,
    creator_id as user_id,
    title,
    presentation_id,
    description,
    openendedmultipleoption,
    subheading,
    maxpoint,
    minpoint,
    hastimelimit,
    showvotes,
    showsubmissions,
    sourceslideid,
    showvotingresultsonaudience,
    request_id,
    user_id as user_id_ai,
    request_content,
    is_ai_slides,
    case when lower("base_type") = 'multiplechoice' then 'Poll'
     when lower("base_type") = 'imagechoice' then 'Poll'
     when lower("base_type") = 'openended' then 'Open Ended'
     when lower("base_type") = 'wordcloud' then 'Word Cloud'
     when lower("base_type") = 'scale' then 'Scales'
     when lower("base_type") = 'ideas' then 'Brainstorm'
     when lower("base_type") = 'question' then 'Q&A'
     when lower("base_type") = 'pickanswer' and lower(slidetype) = 'imagechoice' then 'Pick Image'
     when lower("base_type") = 'pickanswer' and lower(slidetype) = 'typeanswer' then 'Short Answer'
     when lower("base_type") = 'pickanswer' and lower(slidetype) = 'correctorder' then 'Correct Order'
     when lower("base_type") = 'pickanswer' and lower(slidetype) = 'matchpairs' then 'Match Pairs'
     when lower("base_type") = 'pickanswer' and lower(slidetype) = 'categorise' then 'Categorise'
     when lower("base_type") = 'pickanswer' and lower(slidetype) is null then 'Pick Answer'
     when lower("base_type") = 'leaderboard' then 'Quiz Leaderboard'
     when lower("base_type") = 'textslide' then 'Heading'
     when lower("base_type") = 'bulletpoints' then 'List'
     when lower("base_type") = 'qrcode' then 'QR code'
     when lower("base_type") = 'image' then 'Image'
     when lower("base_type") = 'youtube' then 'Youtube'
     when lower("base_type") = 'googleslides' then 'Google Slides'
     when lower("base_type") = 'document' then 'Document'
     when lower("base_type") = 'freestyle' and is_imported_from_ppt = 'true' then 'Document'
     when lower("base_type") = 'freestyle' and is_imported_from_ppt is null then 'Content'
     when lower("base_type") = 'spinnerwheel' then 'Spinner Wheel'
     end as "slidetype_name"
from  {{ ref ('int_slides_final_rv1') }}
