
{{ config(materialized='table', sort='payment_date', unique_key='invoice_id', dist='invoice_id', tags = ["mart"], schema='data_mart',
pre_hook="set statement_timeout to 120000") }}
with int_invoices as (
    select * from {{ ref('int_invoices_b2c') }}
--    where
--        actual_paid > 0

),

fct_invoices as (select
    int_invoices.user_id,
    invoice_id,
    quantity,
--    quantity::float,
    payment_status,
    subscription_plan,
    amount_paid,
    amount_refunded,
    actual_paid,
    currency,
    exchangerate,
    billing_reason,
    plan_type,
    payment_source,
    payment_method,
    subscription_group,
    payment_date,
    coupon_code,
--    subscription_duration_day,
    subscription_end_date,
    order_ranking_asc,
    order_ranking_desc,
    is_updated,
    is_bulk,
    case
        when order_ranking_asc = 1 then 'new_order'
        when
            order_ranking_asc > 1 and billing_reason = 'subscription_cycle'
            then 'renewal'
        when
            order_ranking_asc > 1
            and billing_reason in ('manual', 'subscription_create')
            then 'revived'
        when
            order_ranking_asc > 1 and billing_reason = 'subscription_update'
            then 'update'
    end as current_retention_status,
    case
        when next_billing_reason = 'subscription_cycle' then 'renewal'
        when
            next_billing_reason in ('manual', 'subscription_create')
            then 'revived'
        when next_billing_reason = 'subscription_update' then 'upgrade'
        when
            next_billing_reason is null and subscription_end_date < current_date
            then 'churned'
        when
            next_billing_reason is null
            and subscription_end_date >= current_date
            then 'currently_subscribed'
    end as next_order_status
from int_invoices
where actual_paid > 0
)

select * from fct_invoices