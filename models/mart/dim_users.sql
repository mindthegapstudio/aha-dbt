{{ config(materialized='table',
tags = ["mart"],
schema='data_mart')}}

with
users as (
    select *
    from {{ ref('int_users') }}
    where
        1 = 1
        and is_hacker is false
),

presentation as (
    select * from {{ ref('int_presentations') }}
),

country_region as (
    select * from {{ ref('country_region') }}
),

invoices as (
    select * from {{ ref('int_invoices_b2c') }}
),

events_raw as (
    select * from {{ ref('int_events') }}
),


ld_report as (
    select * from {{ ref('base_ld_import') }}
),

events_old as (
    select
        presentation_owner_id as presenter_id,
        count(distinct presentation_id) as total_events_old
    from
        presentation
    where
        1 = 1
        and online_count >= 4
    group by 1
),

--events_unioned as (
--    select
--        presentation.presentation_id,
--        presentation.presentation_owner_id as presenter_id,
--        events_raw.session_id,
--        presentation.presentation_created_at,
--        presentation_id,
--        presentation.online_count,
--        presentation.online_count_time,
--        events_raw.start_time
--    from
--        presentation
--        left join events_raw
--            on presentation.presentation_id = events_raw.presentation_id
--    where
--        1 = 1
--        and presentation.online_count >= 4
--        and events_raw.online_count >= 4
--),

events_unioned as (
    select
        presenter_id,
        created_at as presentation_created_at,
        presentation_id as presentation_id,
        online_count,
        start_time as presentation_started_at
    from
        events_raw
    where online_count >= 4
    union all
    select
        presentation_owner_id as presenter_id,
        presentation_created_at,
        presentation_id,
        online_count,
        online_count_time as presentation_started_at
    from
        presentation
    where
--        1 = 1
--        and
        online_count >= 4
        and online_count_time is not null
--        and presentation_created_at <= to_date('2022-11-18', 'YYYY-MM-DD')
),

events as (
    select
        presenter_id,
        max(presentation_started_at) as last_event_time,
        count(distinct presentation_id) as events_count,
        min(presentation_started_at) as first_event_time,
--        least(min(online_count_time), min(start_time)) as first_event_time,
        avg(online_count) as average_event_participants,
        max(online_count) as most_event_participants
    from events_unioned
    group by presenter_id
--    , online_count_time, start_time
),

user_invitation as (
    select
        inviter_id,
        count(distinct invitation_id) as total_invite
    from {{ ref('base_user_invitation') }}
    group by 1
),



presentation_summary as (
    select
        presentation_owner_id as presenter_id,
        count(distinct presentation_id) as total_presentation_created,
        bool_or(is_created_from_template) as has_used_template,
        bool_or(is_audience_pacing) as has_use_audience_pacing,
        bool_or(is_changed_font) as has_changed_font,
        bool_or(is_thumbnail_uploaded) as has_uploaded_thumbnail,
        bool_or(is_thumbnail_changed) as has_changed_thumbnail,
        bool_or(is_used_ai) as is_used_ai,
        sum(imagechoice_count) as total_image_choice_slides,
        sum(leaderboard_count) as total_leaderboard_slides,
        sum(matchpairs_count) as total_matchpairs_count,
        sum(multiplechoice_count) as total_multiplechoice_count,
        sum(openended_count) as total_openended_count,
        sum(pickanswer_count) as total_pickanswer_count,
        sum(qrcode_count) as total_qrcode_count,
        sum(scale_count) as total_scale_count,
        sum(spinnerwheel_count) as total_spinnerwheel_count,
        sum(textslide_count) as total_text_slide_co,
        sum(typeanswer_count) as type_answer_count,
        sum(wordcloud_count) as wordcloud_count,
        sum(youtube_count) as youtube_count,
        sum(freestyle_count) as freestyle_count,
        sum(total_slides) as total_slides_created,
        max(presentation_created_at) as last_presentation_created,
        min(presentation_created_at) as presentation_summary_created
    from presentation
    group by 1
),


total_invoices as (
    select
        user_id,
        sum(
            case when payment_status = 'paid' then 1 else 0 end
        ) as total_paid_order,
        sum(
            case
                when
                    payment_status = 'paid' and amount_refunded = 0
                    then 1
                else 0
            end
        ) as total_full_paid_order,
        sum(
            case
                when
                    payment_status = 'paid' and amount_refunded > 0
                    then 1
                else 0
            end
        ) as total_discounted_order,
        sum(
            case when payment_status = 'refunded' then 1 else 0 end
        ) as total_refunded_order,
--        sum(
--            case when is_updated then 0 else subscription_duration_day end
--        ) as total_duration_day,
        sum(amount_paid) as total_amount_paid,
        sum(amount_refunded) as total_amount_refunded,
        min(payment_date) as first_payment_date,
        max(subscription_end_date) as last_subscription_end_date,
        sum(amount_paid - amount_refunded) as total_life_time_value
    from invoices
    group by 1

),

last_invoices as (
    select
        user_id,
        subscription_end_date as last_subscription_end_date,
        subscription_plan as last_subscription,
        subscription_group as last_subscription_group,
        payment_date as last_payment_date
    from invoices
    where order_ranking_desc = 1
),



dim_user as (
    select
        users.user_id,
        users.first_page,
        users.first_page_group::text as first_page_group,
--        users.first_page_group_break_down,
--        users.acquisition_campaign,
        users.use_ahaslides_for,
        users.user_profession,
        users.firstname,
        users.lastname,
        users.team_name,
        users.is_audience,
        users.user_email,
        users.user_domain,
        users.user_domain_type,
        users.sign_up_date,
        users.team_role,
        users.team_id,
        users.team_subscription,
        users.team_plan_endate,
        users.current_plan,
        users.user_country,
        country_region.region,
        country_region."sub-region" as sub_region,
        user_invitation.total_invite as total_invite_sent,
        ld_report.date_to_send_discount_code,
        users.user_city,
        users.is_internal,
        users.is_skoletube,
        users.is_audience_converted,
        users.account_creation_date,
        events_old.total_events_old,
--        users.first_event_date,
        events.first_event_time,
        events.last_event_time,
        events.events_count,
        events.average_event_participants,
        events.most_event_participants,
        last_invoices.last_payment_date,
        last_invoices.last_subscription_end_date,
        last_invoices.last_subscription,
        last_invoices.last_subscription_group,
        presentation_summary.total_presentation_created,
        presentation_summary.has_used_template,
        presentation_summary.has_use_audience_pacing,
        presentation_summary.has_changed_font,
        presentation_summary.has_uploaded_thumbnail,
        presentation_summary.has_changed_thumbnail,
        presentation_summary.total_image_choice_slides,
        presentation_summary.total_leaderboard_slides,
        presentation_summary.total_matchpairs_count,
        presentation_summary.total_multiplechoice_count,
        presentation_summary.total_openended_count,
        presentation_summary.total_pickanswer_count,
        presentation_summary.total_qrcode_count,
        presentation_summary.total_scale_count,
        presentation_summary.total_spinnerwheel_count,
        presentation_summary.total_text_slide_co,
        presentation_summary.type_answer_count,
        presentation_summary.freestyle_count,
        presentation_summary.wordcloud_count,
        presentation_summary.youtube_count,
        presentation_summary.total_slides_created,
        presentation_summary.is_used_ai,
        total_invoices.total_paid_order,
        total_invoices.first_payment_date,
        total_invoices.total_full_paid_order,
        total_invoices.total_discounted_order,
        total_invoices.total_refunded_order,
        total_invoices.total_amount_paid,
        total_invoices.total_amount_refunded,
        total_invoices.total_life_time_value,
--        total_invoices.total_duration_day,
        coalesce(ld_report.user_id is not null, false) as is_ld_report,
        coalesce(
            user_invitation.inviter_id is not null, false
        ) as is_invited_user,
        events.events_count
                / nullif(date_diff('days', users.sign_up_date, current_date),0)
        as average_days_between_events,
        datediff(
            'day',
            users.sign_up_date,
            presentation_summary.presentation_summary_created
        ) as create_presentation_from_signup,
        datediff(
            'day', users.sign_up_date, events.first_event_time
        ) as activation_period,
        datediff(
            'day', users.sign_up_date, total_invoices.first_payment_date
        ) as payment_period,
        datediff(
            'day',
            presentation_summary.presentation_summary_created,
            total_invoices.first_payment_date
        ) as paid_from_presentation,
        datediff(
            'day',
            total_invoices.first_payment_date,
            last_invoices.last_subscription_end_date
        ) as paid_user_stay_duration,
        date_diff(
            'day', events.last_event_time, last_invoices.last_subscription_end_date
        ) as last_event_till_sub_expiration,
        date_diff(
            'day',
            presentation_summary.last_presentation_created,
            last_invoices.last_subscription_end_date
        ) as last_presentation_till_sub_expiration,

--        coalesce(users.source is null, false) as is_organic,
        coalesce(events.presenter_id is not null, false) as is_presenters,
        coalesce(
            presentation_summary.presenter_id is not null, false
        ) as is_presentation,
        coalesce(total_invoices.total_life_time_value > 0, false) as is_paid
    from users
    left join last_invoices
        on users.user_id = last_invoices.user_id
    left join events
        on users.user_id = events.presenter_id
    left join total_invoices
        on users.user_id = total_invoices.user_id
    left join country_region
        on users.user_country = country_region.name
    left join presentation_summary
        on users.user_id = presentation_summary.presenter_id
    left join events_old
        on users.user_id = events_old.presenter_id
    left join ld_report
        on users.user_id = ld_report.user_id
    left join user_invitation
        on users.user_id = user_invitation.inviter_id
)

select * from dim_user

----------------------part mapping with campaigin_id-----------
-- , table_signup as (
--     select
--         user_id,
--         sign_up_date
--     from dim_user
--     where is_audience is false
--     and is_internal is false
-- )

-- , table_usercampaign as (
--     select
--         userid as user_id,
--         campaignid,
--         "timestamp" as event_timestamp
--     from "rds-default-production-aha".public_useradscampaigns
-- ) 

-- , table_campaign as (
--     select
--         *
--     from "rds-default-production-aha".public_adscampaigns
-- )

-- , table_union as (
--     select  user_id, sign_up_date as event_timestamp, null as campaignid
--     from table_signup
--     union all
--     select user_id, event_timestamp, campaignid
--     from table_usercampaign
-- )

-- , lead_time as (
--     select 
--         *, 
--         lead(event_timestamp) over (partition by user_id order by event_timestamp) as lead_event_timestamp,
--         lead(campaignid) over (partition by user_id order by event_timestamp) as lead_campaignid
--     from table_union
-- )

-- , calculate_duration as (
--         select
--             *,
--             case when lead_event_timestamp is not null and lead_campaignid is not null then date_diff('second', lead_event_timestamp, event_timestamp) end as duration
--         from lead_time 
--         -- where  campaignid is null
-- )

--  , limit_time_duration as (
--      select
--          user_id,
--          lead_campaignid,
--          campaignid as union_column,
--          case when duration <= 300 then lead_campaignid end as campaign_id
--      from calculate_duration
--  )
 
--  , filter_table as (
--  select *
--  from limit_time_duration
--  where union_column is null
--  )
 
-- , mapping_uid_and_campaign as (
--  select
--      a.user_id,
--      a.campaign_id,
--      b.source as utm_source
--  from filter_table a 
--  left join table_campaign b 
--  on a.campaign_id = b.id
-- )

-- , final as (
-- select
--     a.*,
--     b.campaign_id,
--     b.utm_sourcedisti
-- from dim_user a 
-- join mapping_uid_and_campaign b 
-- on a.user_id = b.user_id
-- )

-- select count(*) from final where campaign_id  is not null
-- select user_id, count(*) as cnt_row from final group by 1 order by 2 desc 
-- select * from final