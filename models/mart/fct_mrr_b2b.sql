{{ config(materialized='table', sort='month_series', dist='month_series', tags = ["mart"], schema='data_mart',
pre_hook="set statement_timeout to 120000") }}

with raw_b2b_mrr as (
    select
        month_series,
        coalesce(new_mrr,0) + coalesce(recurring_mrr,0) + coalesce(expansion_mrr,0) as total_mrr,
        lead(coalesce(new_mrr,0) + coalesce(recurring_mrr,0) + coalesce(expansion_mrr,0), 1) over (order by month_series desc) as previous_total_mrr,
--        coalesce(new_mrr,0) + coalesce(expansion_mrr,0) + coalesce(churned_mrr,0) as net_new_mrr,
        coalesce(new_mrr,0) as new_mrr,
        coalesce(recurring_mrr,0) as recurring_mrr,
        coalesce(expansion_mrr,0) as expansion_mrr
--        coalesce(churned_mrr,0) as churned_mrr,
--        case
--            when (coalesce(new_mrr,0) + coalesce(recurring_mrr,0) + coalesce(expansion_mrr,0)) = 0 then 0
--            else round(cast(coalesce(-churned_mrr,0) as real) / (coalesce(new_mrr,0) + coalesce(recurring_mrr,0) + coalesce(expansion_mrr,0)),2)
--        end as churn_rate
    from {{ ref('int_raw_mrr_b2b') }}
),

fct_mrr_b2b as (
    select
        month_series,
        total_mrr,
        coalesce(total_mrr,0) - coalesce(previous_total_mrr,0) as net_new_mrr,
        recurring_mrr,
        expansion_mrr,
        new_mrr,
        coalesce(total_mrr,0) - coalesce(previous_total_mrr,0) - expansion_mrr - new_mrr as churned_mrr,
        case
            when coalesce(total_mrr,0) - coalesce(previous_total_mrr,0) - expansion_mrr - new_mrr = 0 then 0
            else round(cast(coalesce((coalesce(total_mrr,0) - coalesce(previous_total_mrr,0) - expansion_mrr - new_mrr),0) as real) / (coalesce(total_mrr,0)),2)
        end as churn_rate
    from raw_b2b_mrr
)

select * from fct_mrr_b2b
order by month_series desc