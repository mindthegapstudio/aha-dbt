{{ config(materialized='table',
tag ='mart',
sort='user_id',
dist='user_id',
schema='data_mart')}}

with ab_test as (
    select * from {{ ref('base_abtest') }}
),

ab_test_users as (
    select *
    from {{ ref('base_abtest_users') }}
),

fct_abtest as (
    select
        ab_test_users.user_id,
        ab_test.test_code,
        ab_test_users.variant,
        ab_test.test_name
    from ab_test_users
    left join ab_test
        on ab_test_users.abtest_id = ab_test.abtest_id
    where
        1 = 1
        and ab_test_users.deletedbyid is null
)

select * from fct_abtest
