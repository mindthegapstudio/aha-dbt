
{{ config(materialized='table') }}

    select distinct cast( createdat as date) as date
    from {{ ref('base_users') }}
    where createdat is not null
    order by date
