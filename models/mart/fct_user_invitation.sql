{{ config(schema='data_mart') }}

with invited_user as (
    select * from {{ ref('base_user_presentations') }}
),

presentation as (
    select * from {{ ref('int_presentations') }}
),

users as (
    select * from {{ ref('int_users') }}
),

invoices as (
    select * from {{ ref('int_invoices_b2c') }}
),

last_invoices as (
    select
        user_id,
        subscription_end_date as last_subscription_end_date,
        subscription_plan as last_subscription,
        payment_date as last_payment_date
    from invoices
    where order_ranking_desc = 1
),



invited_user_status as (
    select
        invited_user.user_id,
        invited_user.presentation_id,
        last_invoices.last_subscription,
        coalesce(
            invited_user.user_id = presentation.presentation_owner_id, false
        ) as is_owner,
        coalesce(last_invoices.last_subscription is not null, false) as is_paid,
        coalesce(users.team_id is not null, false) as is_team
    from invited_user
    left join presentation
        on invited_user.presentation_id = presentation.presentation_id
    left join last_invoices
        on invited_user.user_id = last_invoices.user_id
    left join users
        on invited_user.user_id = users.user_id
)

select * from invited_user_status
