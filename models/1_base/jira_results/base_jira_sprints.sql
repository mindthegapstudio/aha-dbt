with source as (

    select * from {{ source('jira_raw', 'sprint_list') }}

),

renamed as (

    select
        id as sprint_id,
        self as sprint_url,
        state as sprint_status,
        name as sprint_name,
        start_date,
        end_date,
        complete_date,
        origin_board_id,
        board_id,
        goal,
        _dlt_load_id,
        _dlt_id,
        created_date

    from source

)

select * from renamed
