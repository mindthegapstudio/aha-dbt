with source as (

    select * from {{ source('jira_raw', 'velocity_details') }}

),

renamed as (

    select
        sprint_id,
        estimated_points,
        completed_points,
        _dlt_load_id,
        _dlt_id,
        board_id

    from source

)

select * from renamed
