with source as (

    select * from {{ source('b2b_source', 'data') }}

),

renamed as (

    select
        id as user_id,
        email as user_email,
        first_name,
        last_name,
        signup_date,
        plan,
        action,
        _dlt_load_id,
        _dlt_id,
        content_type,
        template_used,
        functions_used,
        date_to_send_discount_code,
        event_started_date,
        hosted_event,
        participants_of_largest_hosted_event,
        note,
        event_started_date__v_text,
        hosted_event__v_text

    from source

)

select * from renamed
