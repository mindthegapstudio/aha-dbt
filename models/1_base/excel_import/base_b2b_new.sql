with source as (

    select * from {{ source('b2b_source', 'b2_b') }}

),

renamed as (

    select
        id,
        company_name,
        contact,
        plan,
        accounts,
        plan_start,
        plan_end,
        plan_start_monthx,
        plan_end_monthx,
        duration_monthx,
        payment_date,
        payment_method,
        transaction_id,
        comment,
        type,
        is_churned,
        team_id,
        is_payment_logged_on_admin,
        _dlt_load_id,
        _dlt_id,
        invoice_link,
        total_amount,
        payment_due,
        commission,
        id__v_text,
        po_link,
        actual_cash_received,
        is_payment_logged_on_admin__v_text,
        team_id__v_text,
        payment_date__v_text

    from source

)

select * from renamed
where id != 0
