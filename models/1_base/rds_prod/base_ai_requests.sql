with source as (

    select *
    from {{ source('rds-default-production-aha', 'public_airequests') }}

),

renamed as (

    select
        id as request_id,
        userid as user_id,
        content,
        type,
        createdbyid,
        createdat,
        updatedbyid,
        updatedat,
        deletedbyid,
        deletedat,
        openaiid,
        openaimodel,
        openaiprompttokenscount,
        openaicompletiontokenscount,
        openaicreatedat,
        isexample

    from source

)

select * from renamed
