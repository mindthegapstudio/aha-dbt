with source as (

    select * from {{ source('rds-default-production-aha', 'public_invoices') }}

),

renamed as (

    select
        invoiceid as invoice_id,
        source as payment_source,
        amount,
        amountrefunded,
        status as payment_status,
        paymentdate,
        currency,
        invoicenumber,
        paymentmethod,
        userid as user_id,
        createdat,
        coupon,
        billingreason,
        note,
        comment,
        ispaid,
        isenterprise,
        userip,
        amountpaid,
        exchangerate,
        amountraw,
        amountrefundedraw,
        amountpaidraw,
        JSON_PARSE(paymentmethod) as payment_parsed,
        JSON_PARSE(lines) as lines_parsed,
        JSON_PARSE(coupon) as coupon_parsed
    from source

)

select
    *,
    payment_parsed.brand as payment_method,
    case
        when lines_parsed[1].amount is not null
            then lines_parsed[1].description
        else lines_parsed[0].description
    end as subscription_type,
    case
        when lines_parsed[0].amount < 0
            then lines_parsed[1].amount
        else lines_parsed[0].amount
    end as original_amount,
    coupon_parsed[0].id as coupon_code,
    lines_parsed[0].discountamount as discount_amount,
    lines_parsed[0].quantity as quantity
from renamed
