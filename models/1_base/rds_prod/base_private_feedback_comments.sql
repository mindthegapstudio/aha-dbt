with source as (

    select *
    from
        {{ source('rds-default-production-aha', 'public_privatefeedbackcomments') }}

),

renamed as (

    select
        id,
        createdby,
        presentationid as presentation_id,
        comment,
        createdat,
        updatedat,
        deleted,
        deletedby,
        deletedat

    from source

)

select * from renamed
