with source as (

    select * from {{ source('rds-default-production-aha', 'public_answers') }}

),

renamed as (

    select
        id as answer_id,
        presentation as presentation_id,
        audience as audience_id,
        vote,
        slide,
        voiceinfo,
        createdat as created_at,
        updatedat as updated_at,
        ipaddress,
        audienceemoji,
        audienceemojicolons,
        audiencename,
        deleted,
        audienceunique,
        timeoutanswer,
        typeanswer,
        audienceteam,
        scale,
        pairs,
        correct,
        globalid,
        deletedat,
        deletedbyid,
        streak,
        laststreak,
        longeststreak,
        streakbonuspoint,
        quiztimestamp

    from source

)

select * from renamed
