with source as (

    select * from {{ source('rds-default-production-aha', 'public_folders') }}

),

renamed as (

    select
        folderid,
        foldername,
        userid,
        modifiedat,
        createdat,
        updatedat,
        globalid,
        accountid,
        description,
        deletedat,
        createdbyid,
        deletedbyid,
        modifiedbyid,
        ownerid,
        parentfolderid,
        path

    from source

)

select * from renamed
