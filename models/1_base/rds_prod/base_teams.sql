with source as (

    select * from {{ source('rds-default-production-aha', 'public_accounts') }}

),

renamed as (

    select
        id as team_id,
        name as team_name,
        createdat as created_at,
        updatedat as updated_at,
        licensequantity,
        planendat as plan_end,
        plan as team_subscription,
        ssodomains,
        samloptions,
        providername,
        createdby,
        createdfrom,
        ownerleftat,
        ownerleftbyid

    from source

)

select * from renamed
