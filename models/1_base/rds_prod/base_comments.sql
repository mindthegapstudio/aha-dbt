with source as (

    select *
    from
        {{ source('rds-default-production-aha', 'public_privatefeedbackcomments') }}

),

renamed as (

    select
        id as comment_id,
        createdby as commenter_id,
        presentationid as presentation_id,
        comment,
        createdat,
        updatedat,
        deleted,
        deletedby,
        deletedat

    from source

)

select * from renamed
