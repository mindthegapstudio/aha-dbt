with source as (

    select * from {{ source('rds-default-production-aha', 'public_roles') }}

),

renamed as (

    select
        id as role_id,
        name as role_name,
        createdat,
        updatedat

    from source

)

select * from renamed
