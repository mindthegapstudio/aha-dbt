with source as (

    select * from {{ source('rds-default-production-aha', 'public_abtests') }}

),

renamed as (

    select
        id as abtest_id,
        testname as test_name,
        testcode as test_code,
        description,
        startdate,
        enddate,
        variantlist,
        nextvariant,
        createdbyid,
        createdat,
        updatedbyid,
        updatedat,
        deletedbyid,
        deletedat

    from source

)

select *
from renamed
