with source as (

    select *
    from {{ source('rds-default-production-aha', 'public_presentations') }}

),

renamed as (

    select
        id as presentation_id,
        name as presentation_name,
        userid as user_id,
        accesscode,
        activeslide,
        modifiedat as modified_at,
        createdat as created_at,
        updatedat as updated_at,
        language,
        ipaddress,
        uniqueaccesscode,
        qrcode,
        sourcepresentation as source_presentation,
        source,
        copycount,
        sharecode,
        presenting,
        lastslide,
        logo as presentation_logo,
        fontfamily as font_family,
        resettimestamp,
        hideinstructionbar,
        onlinecounttime as online_count_time,
        onlinecount as online_count,
        qnaallslide,
        qnaaudienceshowall,
        qnaanonymous,
        showhyperlink,
        hideintrobardocument,
        slideactivetimestamp,
        slidecount,
        notremindcorrectanswer,
        notshowahaslideslogo,
        showahaslideslogo,
        exportdata,
        teamcount,
        teamsize,
        teamscoringrule,
        teamdata,
        folderid,
        enablequizcoundown,
        eventstartedat,
        thumbnailimage as thumbnail_image,
        filteringprofanity,
        sender,
        moderationcode,
        ismoderationmode,
        takenoverby,
        audienceadmission,
        showahaslidescta,
        numberoflikes,
        numberofhearts,
        numberoflaughs,
        numberofsads,
        numberofwows,
        participantscount,
        realtimeonlinecount,
        tags,
        reactions,
        isenablechat as is_chat,
        hasexampleresponses as is_sample_responses,
        enablecopyslidenote,
        areslideoptionsshuffling,
        manualrevealcorrectanswers,
        publicsource as public_source,
        description,
        customthumbnailimage,
        publishedat as published_at,
        crawlsource,
        isindexbot,
        slugifiedname,
        keyword,
        forwho,
        cta,
        enableaudiencerequestpresentation,
        ishideentryspinnerwheen,
        isaudienceauthentication,
        numberofauthens,
        session,
        token,
        expiredtoken,
        hasresults,
        isnewlyadded,
        accountid as team_id,
        isadminpick,
        audiolink,
        audioname as audio_name,
        globalid,
        deletedat as deleted_at,
        deletedbyid,
        disableconversationpresentershare,
        isresizecustomthumbnail,
        coalesce(deleted = 'true', false) as is_deleted,
        coalesce(
            isaudienceauthentication = 'true', false
        ) as is_audience_authentication,
        coalesce(audiencepacing = 'true', false) as is_audience_pacing,
        coalesce(privatemode = 'true', false) as is_private_mode,
        coalesce(teamplay = 'true', false) as is_team_play,
        coalesce(isreactionenabled = 'true', false) as is_reaction,
        coalesce(isenablequizmusic = 'true', false) as is_quiz_music,
        coalesce(ispublicsearch = 'true', false) as is_public_search,
        coalesce(
            ishideindividualleaderboard = 'true',
            false
        ) as is_hide_individual_leaderboard,
        coalesce(
            isenableprivatefeedback = 'true',
            false
        ) as is_private_feedback,
        coalesce(isenableprivatefeedback = 'true', false) as is_streak_bonus

    from source
    where 1 = 1
    qualify row_number() over (
        partition by id order by onlinecounttime asc
    ) = 1

)

select * from renamed
