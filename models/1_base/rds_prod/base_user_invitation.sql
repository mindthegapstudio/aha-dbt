with source as (

    select *
    from {{ source('rds-default-production-aha', 'public_userinvitation') }}

),

renamed as (

    select
        id as invitation_id,
        invitedby as inviter_id,
        invitedemail as invited_email,
        invitedname as invited_name,
        invitedplan as invited_plan,
        expireat,
        sentat as sent_at,
        claimedat as claimed_at,
        claimedby as invited_id,
        createdat as created_at,
        updatedat,
        status,
        accountid as team_id,
        sharedpresentation as presentation_id,
        sharedfolder as folder_id,
        accountrole as team_role

    from source

)

select * from renamed
