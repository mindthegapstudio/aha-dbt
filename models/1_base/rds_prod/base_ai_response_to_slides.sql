with source as (

    select *
    from {{ source('rds-default-production-aha', 'public_airesponsetoslides') }}

),

renamed as (

    select
        id as response_to_slide_id,
        airesponseid as response_id,
        slideid as slide_id,
        createdbyid as requester_id,
        createdat as created_at,
        updatedbyid,
        updatedat,
        deletedbyid,
        deletedat

    from source

)

select * from renamed
