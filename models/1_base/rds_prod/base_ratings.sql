with source as (

    select *
    from
        {{ source('rds-default-production-aha', 'public_privatefeedbackratings') }}

),

renamed as (

    select
        id as rating_id,
        createdby as rater_id,
        presentationid as presentation_id,
        rating,
        createdat,
        updatedat

    from source

)

select * from renamed
