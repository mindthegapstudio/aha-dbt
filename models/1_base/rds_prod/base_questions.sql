with source as (

    select * from {{ source('rds-default-production-aha', 'public_questions') }}

),

renamed as (

    select
        id as question_id,
        deleted,
        createdat as created_at,
        updatedat as updated_at,
        audience as audience_id,
        vote,
        title,
        pinned,
        votecount,
        audiencename as audience_name,
        audienceemoji,
        audiencecolor,
        presentationid as presentation_id,
        answeredtimestamp,
        answered as is_answered,
        status,
        isprivate,
        globalid

    from source

)

select * from renamed
