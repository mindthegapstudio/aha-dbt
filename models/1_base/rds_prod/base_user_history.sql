with source as (

    select *
    from {{ source('rds-default-production-aha', 'public_userhistory') }}

),

renamed as (

    select
        id as history_id,
        userid as user_id,
        action as user_action,
        description,
        meta,
        createdat,
        updatedat,
        ref

    from source

)

select * from renamed
