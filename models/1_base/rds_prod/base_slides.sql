{{ config(materialized='view',
 tags = ["base"]) }}

with source as (

    select * from {{ source('rds-default-production-aha', 'public_slides') }}

),

renamed as (

    select
        id as slide_id,
        type as base_type,
        title,
        description,
        deleted as is_deleted,
        typechart,
        multiplechoice,
        -- voiceactive,
        hideresult,
        stopsubmission,
        presentationid as presentation_id,
        limitchoice,
        showcorrectoption,
        addcorrectoption,
        createdby as creator_id,
        layout,
        openendedmultipleoption,
        createdat as created_at,
        updatedat as updated_at,
        subheading,
        bulletpointsindex,
        showallbulletpoints,
        imagecaption,
        fastanswergetmorepoint,
        -- image,
        -- imagetype,
        -- backgroundimage,
        -- youtubelink,
        -- basecolour,
        -- showpercentage,
        -- textcolour,
        -- visibility,
        -- entriesperparticipant,
        -- showquestionimage,
        -- questionindex,
        -- questioncount,
        -- quizstatus,
        -- timetoanswer,
        titledesc,
        -- quiztimestamp,
        slidetype,
        iscorrectgetpoint,
        timestampleaderboard,
        slidetimestamp,
        additionalfields,
        textalign,
        correctquiztypeanswer,
        othercorrectquiz,
        maxpoint,
        minpoint,
        hastimelimit,
        stopsubmissiontime,
        metadata,
        -- questionimagecropperdata,
        -- originquestionimage,
        -- backgroundimagecropperdata,
        -- originbackgroundimage,
        -- publishedlink,
        -- showiframe,
        -- showyoutubeiframe,
        -- scale,
        -- backgroundsize,
        showvotes,
        -- resettimestamp,
        -- ideascount,
        -- votingstep,
        showsubmissions,
        showvotingresultsonaudience,
        imagesubmission,
        sourceslideid,
        -- audiovolume,
        -- audiomuted,
        titlehtml,
        canvasblocksurl,
        ispublictemplate,
        deletedat as deleted_at,
        -- randomcolours,
        -- globalid,
        -- contenttemplatethumbnail,
        deletedbyid as deleted_by_id,
        numberofhintsshown,
        isgroupwordcloudwords,
        numberofwordsingroup,
        lasttimegroupwordcloud,
        numberofuniquewordsingroup,
        case
            when
                type = 'pickAnswer' and slidetype = 'imageChoice'
                then 'imageChoice'
            when
                type = 'pickAnswer' and slidetype = 'correctOrder'
                then 'correctOrder'
            when
                type = 'pickAnswer' and slidetype = 'matchPairs'
                then 'matchPairs'
            when
                type = 'pickAnswer' and slidetype = 'typeAnswer'
                then 'typeAnswer'
            else type
        end as slide_type,
        json_parse(wordcloudsmartgrouping) as wordcloudsmartgrouping

    from source

)

select * from renamed
