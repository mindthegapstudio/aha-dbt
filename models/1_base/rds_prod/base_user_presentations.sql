with source as (

    select *
    from {{ source('rds-default-production-aha', 'public_userpresentations') }}

),

renamed as (

    select
        createdat,
        updatedat,
        userid as user_id,
        presentationid as presentation_id

    from source

)

select * from renamed
