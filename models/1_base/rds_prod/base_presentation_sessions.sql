with source as (

    select *
    from
        {{ source('rds-default-production-aha', 'public_presentationsessions') }}

),

renamed as (

    select
        id as session_id,
        presentationid as presentation_id,
        starttime as start_time,
        participantcount as participant_count,
        onlinecount as online_count,
        userplan as user_subscription,
        createdat as created_at,
        updatedat as updated_at,
        presenterid as presenter_id,
        source,
        case
            when not is_valid_json(presentationsetting) then null else
                json_parse(lower(presentationsetting))
        end as session_settings,
        is_valid_json(presentationsetting) as is_parsable

    from source
    where 1 = 1

)

select
    *,
    session_settings.name as presentation_name, 
    session_settings.fontfamily as font_family,
    session_settings.slidecount as slide_count,
    session_settings.qnaallslide as is_qna_all_slide,
    session_settings.isenablechat as is_enable_chat,
    session_settings.qnaanonymous as is_anonymous_qna,
    session_settings.showhyperlink as is_show_hyperlink,
    session_settings.audiencepacing as is_audience_pacing,
    session_settings.numberofauthens as number_of_authentication_code_generated,
    session_settings.teamscoringrule as team_scoring_rule,
    session_settings.ismoderationmode as is_moderated_audience_question,
    session_settings.showahaslidescta as is_show_ahaslides_cta,
    session_settings.isenablequizmusic as is_enable_quiz_music,
    session_settings.isreactionenabled as is_reaction_enabled,
    session_settings.showahaslideslogo as is_show_ahaslides_logo,
    session_settings.enablequizcoundown as is_quiz_countdown_enabled,
    session_settings.filteringprofanity as is_filtering_profanity,
    session_settings.hideinstructionbar as is_hide_instruction_bar,
    session_settings.qnaaudienceshowall as is_qna_audience_show_all,
    session_settings.isaudienceauthentication as is_audience_authentication,
    session_settings.enablecopyslidenote as is_enable_copy_slide_note,
    session_settings.hasexampleresponses as is_has_example_responses,
    session_settings.customthumbnailimage as is_custom_thumbnail,
    session_settings.hideintrobardocument as is_hide_intro_bar_document,
    session_settings.notshowahaslideslogo as is_hide_ahaslides_logo,
    session_settings.notremindcorrectanswer as is_not_remind_correct_answer,
    -- -- isHideEntrySpinnerWheen
    session_settings.areslideoptionsshuffling as is_slide_option_shuffle,
    session_settings.manualrevealcorrectanswers as is_manual_correct_answers
-- --   isHideIndividualLeaderboard
-- --   enableAudienceRequestPresentation

from renamed
