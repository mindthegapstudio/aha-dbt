with source as (

    select *
    from
        {{ source('rds-default-production-aha', 'public_userrolepresentations') }}

),

renamed as (

    select
        id,
        userid as invited_id,
        roleid as role_id,
        presentationid as presentation_id,
        createdbyid as inviter_id,
        createdat as created_at,
        updatedbyid,
        updatedat,
        deletedbyid,
        deletedat
    from source

)

select * from renamed
