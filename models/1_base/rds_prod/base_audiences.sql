with source as (

    select * from {{ source('rds-default-production-aha', 'public_audiences') }}

),

renamed as (

    select
        id,
        audienceid as audience_id,
        organisation,
        name,
        email,
        presentationid as presentation_id,
        createdat as created_at,
        updatedat,
        deleted,
        isrequestpresentation,
        emailrequested,
        deletedat,
        deletedbyid,
        userid,
        emoji,
        color,
        isfeedbackemailsent,
        isanonymous

    from source

)

select * from renamed
