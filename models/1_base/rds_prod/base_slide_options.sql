with source as (

    select *
    from {{ source('rds-default-production-aha', 'public_slideoptions') }}

),

renamed as (

    select
        id as option_id,
        title,
        slideid as slide_id,
        correct,
        deleted,
        createdby,
        presentationid,
        audience,
        createdat,
        updatedat,
        image,
        imagecropped,
        audienceunique,
        audiencename,
        audienceteam,
        audienceemoji,
        audienceemail,
        lastcropperdata,
        votescount,
        pairtitle,
        globalid,
        randomindexid

    from source

)

select * from renamed
