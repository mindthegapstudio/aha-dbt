with source as (

    select *
    from {{ source('rds-default-production-aha', 'public_airesponses') }}

),

renamed as (

    select
        id as response_id,
        airequestid as request_id,
        createdbyid as requester_id,
        createdat as created_at,
        updatedbyid,
        updatedat as updated_at,
        deletedbyid,
        deletedat,
        responsetype,
        json_parse(lower(content)) as content

    from source

)

select
    *,
    coalesce(
        content.questiontitle,
        content.data,
        content.slideoptions,
        content.content
    ) as response_content
from renamed
