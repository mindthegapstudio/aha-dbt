with source as (

    select * from {{ source('rds-default-production-aha', 'public_users') }}

),

renamed as (

    select
        id as user_id,
        firstname,
        lastname,
        password,
        email,
        case
            when email is not null then split_part(email::text, '@', 2)
            else null
        end as domain,
        salt,
        active,
        avatar,
        role,
        createdat,
        updatedat,
        source,
        signupipaddress,
        plantimeend,
        plan,
        stripecustomerid,
        referrer,
        onetimepaymentid,
        onetimeend,
        whitelabellink,
        identifyprovider,
        provideruserid,
        provideruseremail,
        whitelabelpresenterdomain,
        whitelabelfavicon,
        whitelabeltitle,
        remembersettings,
        stripesource,
        address,
        whitelabelogimage,
        lastuserip,
        accountrole as team_role,
        accountid as team_id,
        accountdata,
        preferences,
        isguest,
        loggedoutat,
        playedpresentationids,
        country,
        referraluserid,
        referralawardstatus,
        referralcode,
        firsteventstartedat,
        type,
        city,
        firstpage,
        lasteventstartedat,
        personalinfo,
        linkedinlink,
        isdisablepublictemplate,
        isnoindex,
        slidegeneratorrequestcount,
        deletedat,
        deletedbyid,
        notificationreadtime,
        timemarkreadallnotification,
        currency,
        presenterstartedat,
        json_parse(lower(personalinformation)) as personal_info

    from source

)

select * from renamed
