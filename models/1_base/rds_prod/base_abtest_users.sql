with source as (

    select *
    from {{ source('rds-default-production-aha', 'public_abtestusers') }}

),

renamed as (

    select
        id,
        variant,
        userid as user_id,
        abtestid as abtest_id,
        createdbyid,
        createdat,
        updatedbyid,
        updatedat,
        deletedbyid,
        deletedat

    from source

)

select * from renamed
