with source as (

    select * from {{ source('ga4_data_v2', 'ga4_events_new') }}

),

renamed as (

    select
        date::DATE as event_date,
        event_name,
        country,
        event_count_integer,
        _dlt_load_id,
        _dlt_id,
        row_number()
            over (
                partition by event_name, country, date
                order by event_count_integer desc
            )
        as rn,
        {{ dbt_utils.generate_surrogate_key(['event_name', 'country','event_date']) }} as unique_key
    from source


)

select * from renamed
where rn = 1
