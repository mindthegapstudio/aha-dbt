
with source as (

    select * from {{ source('ga4_data_v2', 'ga4_new_test') }}

),

renamed as (

    select
        "date"::DATE as traffic_date,
        country,
        first_user_default_channel_group,
        first_user_campaign_name,
        first_user_campaign_id,
        first_user_medium,
        first_user_source,
        active_users_integer,
        sessions_integer,
        _dlt_load_id,
        row_number()over (partition by date, country, first_user_default_channel_group,  first_user_campaign_name,   first_user_campaign_id,    first_user_medium,  first_user_source order by  sessions_integer desc ) rn,
        _dlt_id

    from source
    qualify 


)

select * from renamed
where rn = 1
