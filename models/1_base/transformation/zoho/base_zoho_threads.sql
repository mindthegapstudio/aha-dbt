with source as (

    select * from {{ source('zoho', 'zoho_threads') }}

),

renamed as (

    select
        id,
        channel,
        can_reply,
        content_type,
        has_attach,
        status,
        summary,
        author__id,
        author__name,
        author__email,
        author__type,
        author__last_name,
        attachment_count,
        is_description_thread,
        source__type,
        visibility,
        created_time,
        direction,
        cc,
        bcc,
        "to" as send_to,
        from_email_address,
        is_forward,
        ticket_id,
        _dlt_load_id,
        _dlt_id,
        author__photo_url,
        author__first_name,
        responded_in,
        responder_id

    from source

)

select * from renamed

