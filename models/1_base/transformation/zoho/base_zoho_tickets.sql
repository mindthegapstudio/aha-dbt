
with source as (

    select * from {{ source('zoho', 'zoho_tickets') }}

),

renamed as (

    select
        id as ticket_id,
        modified_time,
        status_type,
        subject,
        department_id,
        channel,
        language,
        source__type,
        approval_count,
        is_over_due,
        is_trashed,
        contact__last_name,
        contact__id,
        contact__is_spam,
        contact__email,
        created_time,
        is_response_overdue,
        customer_response_time,
        contact_id,
        thread_count,
        comment_count,
        task_count,
        web_url,
        assignee__photo_url,
        assignee__first_name,
        assignee__last_name,
        assignee__id,
        assignee__email,
        is_spam,
        status,
        ticket_number,
        is_archived,
        time_entry_count,
        is_deleted,
        modified_by,
        follower_count,
        email,
        layout_details__id,
        layout_details__layout_name,
        is_following,
        layout_id,
        assignee_id,
        created_by,
        tag_count,
        attachment_count,
        is_escalated,
        _dlt_load_id,
        _dlt_id,
        custom_fields__label,
        cf__cf_label,
        closed_time,
        contact__first_name,
        description,
        due_date,
        priority,
        sla_id,
        contact__phone,
        contact__mobile

    from source

)

select * from renamed

