
with source as (

    select * from {{ source('zoho', 'zoho_reviews') }}

),

renamed as (

    select
        ticket_id,
        feedback,
        rated_time,
        agent_id,
        customer_rated_time,
        contact_id,
        timezone_offset,
        department_id,
        rating,
        id,
        _dlt_load_id,
        _dlt_id

    from source

)

select * from renamed

