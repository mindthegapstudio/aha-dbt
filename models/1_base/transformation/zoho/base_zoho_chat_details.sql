with source as (

    select * from {{ source('zoho', 'zoho_chat_details') }}

),

renamed as (

    select
        chat_id,
        operator_id,
        is_bot,
        start_time,
        attended_time,
        end_time,
        unread_chats,
        chat_status,
        _dlt_load_id,
        _dlt_id,
        operator_email,
        missed_time

    from source

)

select * from renamed

