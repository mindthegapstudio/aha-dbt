with source as (

    select * from {{ source('ai_language', 'ai_language') }}

),

renamed as (

    select
        request_date,
        input_language,
        output_language,
        _dlt_load_id,
        _dlt_id,
        request_id,
        response_id,
        row_number()
            over (
                partition by response_id, request_id, request_date
                order by request_date desc
            )
        as rn

    from source

)

select * from renamed
where rn = 1
