with whitelist as (
    select * from {{ ref('whitelist_license_transfer') }}
)

select distinct
    user_id,
    LISTAGG(user_group, ', ')
    within group (order by user_group)
        over (partition by user_id)
    as user_group
from whitelist

order by user_id
