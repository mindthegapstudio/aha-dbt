with source as (

    select * from {{ source('similar_web_source', 'classpoint') }}
    union all
    select * from {{ source('similar_web_source', 'classpoint2') }}

),

renamed as (

    select
        time_period,
        domain,
        'classpoint' as company_name,
        channel,
        traffic_share,
        channel_traffic,
        _dlt_load_id,
        _dlt_id

    from source

)

select * from renamed
