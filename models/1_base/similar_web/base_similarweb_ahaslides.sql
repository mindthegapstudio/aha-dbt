with source as (

    select * from {{ source('similar_web_source', 'ahaslides') }}
    -- union all 
    -- select * from {{ source('similar_web_source', 'classpoint') }}
    -- union all 
    -- select * from {{ source('similar_web_source', 'classpoint2') }}
    -- union all 
    -- select * from {{ source('similar_web_source', 'kahoot1') }}
    -- union all 
    -- select * from {{ source('similar_web_source', 'kahoot2') }}
    -- union all 
    -- select * from {{ source('similar_web_source', 'pollev') }}
    -- union all 
    -- select * from {{ source('similar_web_source', 'polleverywhere') }}
    -- union all 
    -- select * from {{ source('similar_web_source', 'slido') }}
    -- union all 
    -- select * from {{ source('similar_web_source', 'slido2') }}
    -- union all 
    -- select * from {{ source('similar_web_source', 'quizziz') }}

),

renamed as (

    select
        time_period,
        domain,
        'ahaslides' as company_name,
        channel,
        traffic_share,
        channel_traffic,
        _dlt_load_id,
        _dlt_id

    from source

)

select * from renamed
