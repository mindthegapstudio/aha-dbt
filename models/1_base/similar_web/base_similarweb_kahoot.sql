with source as (

    select * from {{ source('similar_web_source', 'kahoot1') }}
    union all
    select * from {{ source('similar_web_source', 'kahoot2') }}

),

renamed as (

    select
        time_period,
        domain,
        'kahoot' as company_name,
        channel,
        traffic_share,
        channel_traffic,
        _dlt_load_id,
        _dlt_id

    from source

)

select * from renamed
