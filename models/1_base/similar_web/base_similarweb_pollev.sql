with source as (

    select * from {{ source('similar_web_source', 'pollev') }}
    union all
    select * from {{ source('similar_web_source', 'polleverywhere') }}

),

renamed as (

    select
        time_period,
        domain,
        'pollev' as company_name,
        channel,
        traffic_share,
        channel_traffic,
        _dlt_load_id,
        _dlt_id

    from source

)

select * from renamed
