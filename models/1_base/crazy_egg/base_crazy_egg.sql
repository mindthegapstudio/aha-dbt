with source as (

    select * from {{ source('crazyegg_survey', 'satisfcation_survey') }}

),

renamed as (

    select
        visitor_id as user_id,
        url,
        browser,
        device,
        country,
        _1_how_was_your_last_presentationx::float as ratings,
        _dlt_load_id,
        _dlt_id,
        _2_share_us_your_thoughts_sparklesx as comments,
        _2_sentiment as sentiment,
        to_timestamp(
            started_survey_on, 'YYYY-MM-DD HH24:MI:SS'
        ) as started_survey_on

    from source

)

select * from renamed
