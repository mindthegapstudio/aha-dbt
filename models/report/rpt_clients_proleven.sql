
{{ config(materialized='incremental', unique_key = 'session_id', tags = ["report"], schema='data_mart') }}
with events as (
    select * from {{ ref('int_events') }}
    {% if is_incremental() %}
        where updated_at >= (select date_add('day', -1, current_date))
    {% endif %}
    
),


users as (
    select * from {{ ref('int_users') }}


),

slides as (
    select * from {{ ref('int_slides_final') }}
),

presentation as(
    select * from {{ ref('int_presentations') }}
),

slides_total as (
    select
        presentation_id,
        bool_or(is_ai_slides) as is_used_ai
    from slides
    group by 1
),

fct_events as (

    select
        events.session_id,
        presentation.presentation_name,
        'https://presenter.ahaslides.com/presentation/'||presentation.presentation_id::VARCHAR as presentation_link,
        events.presentation_id,
        events.source,
        events.presenter_id,
        events.start_time,
        events.online_count,
        events.participant_count,
        events.user_subscription,
        users.team_id, 
        users.user_id,
        users.user_email,
        users.firstname ||' '|| users.lastname user_name,
        users.firstname,
        users.lastname,
        events.slide_count,
        lag(events.start_time)
            over (
                partition by
                    events.presenter_id
                order by events.start_time
            )
        as last_event_time,
        case
            when (
                users.team_id is not null
                and users.team_plan_endate > events.start_time
            )
                then users.team_subscription
            else events.user_subscription
        end as hosting_subscription
    from events
    left join users
        on events.presenter_id = users.user_id
    left join slides_total
        on events.presentation_id = slides_total.presentation_id
    left join presentation 
        on events.presentation_id = presentation.presentation_id
 
)

select
    *
from fct_events
where 1=1 
    and team_id = 9332

