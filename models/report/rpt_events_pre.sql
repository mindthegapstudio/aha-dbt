
{{ config(materialized='table', sort='event_date', dist='event_date', tags = ["report"], schema='data_mart',
pre_hook="set statement_timeout to 120000") }}
with events as (
    select * from {{ ref('fct_events') }}
),

count_event as (
    select
        start_time::date as event_date,
        count(distinct session_id) as total_events_pre
    from events
    where 1 = 1
    group by 1
),

yoy_comparision as (
    select
        count_event.event_date,
        count_event.total_events_pre,
        last_year.total_events_pre as yoy_total_events_pre
    from count_event
    left join count_event as last_year
        on count_event.event_date = date_add('year', 1, last_year.event_date)

)

select * from yoy_comparision
