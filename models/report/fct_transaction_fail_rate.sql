
{{ config(materialized='table', sort='user_id', dist='transaction_date', tags = ["report"], schema='data_mart',
pre_hook="set statement_timeout to 120000") }}
with invoices as (
    select * from {{ ref('int_invoices_b2c') }}
),

payment_failure as (
    select * from {{ ref('int_payment_failure') }}
),



event_processed as (
    select
        coalesce(invoices.user_id, payment_failure.user_id) as user_id,
        coalesce(
            invoices.payment_date, payment_failure.createdat
        ) as transaction_date,
        count(distinct invoices.invoice_id) as total_success_order,
        count(distinct payment_failure.payment_failure_id) as total_failed_order
    from invoices full outer join payment_failure
        on
            invoices.user_id = payment_failure.user_id
            and invoices.payment_date = payment_failure.createdat
    group by 1, 2
)


select * from event_processed
