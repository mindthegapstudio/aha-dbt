
{{ config(materialized='table', sort='event_date', dist='event_date', tags = ["report"], schema='data_mart') }}
with events as (
    select * from {{ ref('fct_events_by_presentation') }}
),

users as (
    select * from {{ ref('int_users') }}
),

count_event as (
    select
        trunc(online_count_time) as event_date,
        count(distinct presentation_id) as total_events_pre
    from events
    where 1 = 1
    group by 1
),

yoy_comparision as (
    select
        count_event.event_date,
        count_event.total_events_pre,
        last_year.total_events_pre as yoy_total_events_pre
    from count_event
    left join count_event as last_year
        on count_event.event_date = date_add('year', 1, last_year.event_date)

)

select * from yoy_comparision
