
{{ config(materialized='table',
sort='sign_up_date',
tags = ["report"],
schema='data_mart')}}
with
users as (
    select * from {{ ref('int_users') }}
    where
        1 = 1
        and is_hacker is false
        and is_internal is false
        and is_audience is false
        and user_email is not null
),

first_slide as (
    select
        presentation_owner_id,
        date_trunc(
            'day', presentation_created_at
        ) as first_presentation_created,
        min(presentation_created_at) as min_presentation
    from {{ ref('int_presentations') }}
    where
        1 = 1
    group by 1, 2
),

first_event as (
    select
        presentation_owner_id as presenter_id,
        date_trunc('day', online_count_time) as first_event_time,
        min(online_count_time) as first_events
    from {{ ref('int_presentations') }}
    where
        1 = 1
        and online_count >= 4
    group by 1, 2
),


invoices as (
    select
        user_id,
        payment_date
    from {{ ref('int_invoices_b2c') }}
    where
        actual_paid > 0
        and order_ranking_asc = 1
),

day_from as (
    select
        users.user_id,
        users.sign_up_date,
        date_add('year', -1, sign_up_date) as same_date_last_year,
        coalesce(
            first_slide.presentation_owner_id is not null, false
        ) as is_created_presentation,
        coalesce(first_event.presenter_id is not null, false) as is_hosted,
        coalesce(
            (invoices.user_id, users.team_id) is not null, false
        ) as is_paid,
        datediff(
            'day', sign_up_date, first_presentation_created
        ) as create_presentation_from_signup,
        datediff('day', sign_up_date, first_event_time) as event_from_signup,
        datediff('day', sign_up_date, payment_date) as paid_from_sign_up,
        datediff(
            'day', first_presentation_created, payment_date
        ) as paid_from_presentation
    from users
    left join first_event on users.user_id = first_event.presenter_id
    left join invoices on users.user_id = invoices.user_id
    left join first_slide on users.user_id = first_slide.presentation_owner_id
),

this_year as (

    select
        trunc(sign_up_date) sign_up_date,
        trunc(same_date_last_year) same_date_last_year,
        count(distinct user_id) as total_sign_up,
        count(
            distinct case when paid_from_sign_up <= 7 then user_id end
        ) as total_paid_from_signup_7days,
        count(
            distinct case when event_from_signup <= 7 then user_id end
        ) as total_event_from_signup_7days,
        count(
            distinct case when is_created_presentation is true then user_id end
        ) as total_created_presentation_users,
        count(
            distinct case when is_hosted is true then user_id end
        ) as total_hosted_event_user,
        count(
            distinct case when is_paid is true then user_id end
        ) as total_paid_user
    from day_from
    group by 1, 2
)

select
    this_year.*,
    last_year.total_paid_from_signup_7days as yoy_total_paid_from_signup_7days,
    last_year.total_event_from_signup_7days as yoy_total_event_from_signup_7days,
    last_year.total_sign_up as yoy_total_sign_up,
    last_year.total_created_presentation_users as yoy_total_created_presentation_users,
    last_year.total_hosted_event_user as yoy_total_hosted_event_user,
    last_year.total_paid_user as yoy_total_paid_user
from this_year left join this_year as last_year
    on this_year.same_date_last_year = last_year.sign_up_date
