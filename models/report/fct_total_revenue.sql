
{{ config(materialized='table', tags = ["mart"], schema='data_mart',
pre_hook="set statement_timeout to 120000") }}
with b2c as (
    select
        payment_date,
        subscription_plan,
        subscription_end_date,
        'b2c' as invoice_type,
        actual_paid
    from {{ ref('int_invoices_b2c') }}
    where actual_paid > 0
),

b2b as (
    select
        plan_start,
        plan,
        plan_end,
        'b2b' as invoice_type,
        total_amount as actual_paids
    from {{ ref('base_b2b_new') }}
)

select * from b2c
union all
select * from b2b
