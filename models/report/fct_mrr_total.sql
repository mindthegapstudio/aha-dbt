{{ config(materialized='table', sort='month_series', dist='month_series', schema='data_mart', tags = ["report"],
pre_hook="set statement_timeout to 120000") }}

with b2c as (
    select * from {{ ref('fct_mrr_b2c') }}
),

b2b as (
    select * from {{ ref('fct_mrr_b2b') }}
),

total as (
    select
        b2c.month_series,
        coalesce(b2c.total_mrr,0) + coalesce(b2b.total_mrr,0) as total_mrr,
        coalesce(b2c.total_mrr,0) as total_mrr_b2c,
        round(cast(coalesce(b2c.total_mrr,0) as real) / (coalesce(b2c.total_mrr,0) + coalesce(b2b.total_mrr,0))*100,2) as b2c_contribution,
        coalesce(b2b.total_mrr,0) as total_mrr_b2b,
        round(100 - (cast(coalesce(b2c.total_mrr,0) as real) / (coalesce(b2c.total_mrr,0) + coalesce(b2b.total_mrr,0))*100),2) as b2b_contribution,
        coalesce(b2c.prosumer_mrr,0) as prosumer_mrr_b2c,
        coalesce(b2c.one_time_mrr,0) as one_time_mrr_b2c,
        coalesce(b2c.prosumer_net_new,0) as prosumer_net_new_b2c,
        coalesce(b2c.prosumer_new_mrr,0) as prosumer_new_mrr_b2c,
        coalesce(b2c.prosumer_expansion_mrr,0) as prosumer_expansion_mrr_b2c,
        coalesce(b2c.prosumer_recurring_mrr,0) as prosumer_recurring_mrr_b2c,
        coalesce(b2c.prosumer_churn_mrr,0) as prosumer_churn_mrr_b2c,
        coalesce(b2b.net_new_mrr,0) as net_new_mrr_b2b,
        coalesce(b2b.new_mrr,0) as new_mrr_b2b,
        coalesce(b2b.recurring_mrr,0) as recurring_mrr_b2b,
        coalesce(b2b.expansion_mrr,0) as expansion_mrr_b2b,
        coalesce(b2b.churned_mrr,0) as churn_mrr_b2b
from b2c
    left join b2b
        on b2c.month_series = b2b.month_series
)

select * from total