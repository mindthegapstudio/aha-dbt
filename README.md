Welcome to your new dbt project!
### Setting up your environment 
Clone this project from gitlab 

Create a Python Virtual Environment ( if you use other Python library) -> Activate the virtual env
Run pip install -r requirements.txt

Create a .dbt folder in your home directory -> Move profiles.yml file to .dbt folder

Download the PEM keys to the SSH instance ( Ask Don/ Karl/ Rob for access if this is not yet avaialble to you)

sudo ssh -i < .pem path> -L 5439:datawarehouse-prd.cps9bf1es8tr.us-east-1.redshift.amazonaws.com:5439 ubuntu@34.225.242.45

run dbt debug to confirm that your setup is ok. If All checks passed! and connection test return as [OK connection ok], you should be good to go


### Using the starter project

Try running the following commands:
- dbt run
- dbt test


### Resources:
- Learn more about dbt [in the docs](https://docs.getdbt.com/docs/introduction)
- Check out [Discourse](https://discourse.getdbt.com/) for commonly asked questions and answers
- Join the [chat](https://community.getdbt.com/) on Slack for live discussions and support
- Find [dbt events](https://events.getdbt.com) near you
- Check out [the blog](https://blog.getdbt.com/) for the latest news on dbt's development and best practices
