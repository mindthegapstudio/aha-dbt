FROM python:3.9.18-slim

# Update and install system packages
RUN apt-get update -y && \
  apt-get install --no-install-recommends -y -q \
  git libpq-dev && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set environment variables
ENV DBT_DIR /dbt

# Set working directory
WORKDIR $DBT_DIR

# Copy requirements
COPY . /dbt

# Install DBT
RUN pip install -U pip && pip install -r requirements.txt && dbt deps

# Add dbt_project_1 to the docker image
 
EXPOSE 5439