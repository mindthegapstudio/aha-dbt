{% macro grant_permissions(schema, role) %}
  -- Grant usage on the schema
  GRANT USAGE ON SCHEMA {{ schema }} TO "{{ role }}";

  -- Grant select on all existing tables in the schema
  GRANT SELECT ON ALL TABLES IN SCHEMA {{ schema }} TO "{{ role }}";

  -- Grant select on future tables in the schema
  ALTER DEFAULT PRIVILEGES IN SCHEMA {{ schema }}
  GRANT SELECT ON TABLES TO "{{ role }}";
{% endmacro %}
